#define IDMLOG_FATAL printf
#define IDMLOG_ERROR printf
#define IDMLOG_WARNING	printf
#define IDMLOG_INFO printf
#define IDMLOG_TRACE printf


#include "GL/glew.h"
#include <IDMEngine.h>
#include "Renderer.h"
#include <resources/idm/IDMLoader.h>
#include <scenegraph/IDMScene.h>
#include <scenegraph/IDMImage.h>

//====================================
#undef DrawText
#include <scenegraph/IDMText.h>
//====================================

#include <exporters/IDMYUVExporter.h>
#include <exporters/IDMImageExporter.h>
#include <concurrency/ThreadUtil.h>
#include <IdmTextManager.h>
#include <utils/StringUtils.h>

//#include <IDMLogging.h>


using namespace idm::cv;


//=============================================================================
bool CmdSetAsset::operator()(void){
	return m_renderer->SetAsset(m_id, m_assetInfo.c_str());
}


//=============================================================================
bool CmdSetFps::operator()(void){
	m_renderer->SetFps(m_fps);
	return true;
}


//=============================================================================
bool CmdSetState::operator()(void){
	m_renderer->SetState(m_state);
	return true;
}

//=============================================================================
bool CmdSetFrameId::operator()(void){
	m_renderer->SetFrameId(m_frameId);
	return true;
}

//=============================================================================
bool CmdLoadIdm::operator()(void){
	return m_renderer->LoadIdm(m_idmPath.c_str());
}


//=============================================================================
Renderer::Renderer(const char* pipeName, OutputType type, bool isSequence )
	: m_init(false)
	, m_view(nullptr)
	, m_loader(nullptr)
	, m_exporter(nullptr)
    , m_pipeName(pipeName)
    , m_outputType(type)
    , m_isSequence(isSequence)
	, m_ready(false)
	, m_done(false)
	, m_frameId(0)
	, m_fps(-1.0)
{
}


//=============================================================================
Renderer::~Renderer()
{
	m_cmdQueue.Destroy();
	m_doneQueue.Destroy();
}

//=============================================================================
bool Renderer::Start()
{
	//IDMLOG_INFO("[Renderer][0x%x]: Start called from thread.\n", std::this_thread::get_id().hash());
	IDMLOG_INFO("[Renderer][0x%x]: Start called from thread.\n", std::hash<std::thread::id>()(std::this_thread::get_id()));
	m_renderThread = std::thread(&Renderer::Worker, this);
	IDMConcurrency::SetThreadName(m_renderThread.native_handle(), "render_thread");
	//IDMLOG_INFO("[Renderer][0x%x]: Thread started with id.\n", m_renderThread.get_id().hash());
	IDMLOG_INFO("[Renderer][0x%x]: Thread started with id.\n", std::hash<std::thread::id>()(m_renderThread.get_id()));
	//m_renderThread.get_id().id()
	m_ready = true;
	return true;
}

//=============================================================================
void Renderer::Stop()
{
	//IDMLOG_INFO("[Renderer][0x%x]: Stop called from thread.\n", std::this_thread::get_id().hash());
	IDMLOG_INFO("[Renderer][0x%x]: Stop called from thread.\n", std::hash<std::thread::id>()(std::this_thread::get_id()) );
	m_done = true;
	m_ready = false;
	m_renderThread.join();
	//IDMLOG_INFO("[Renderer][0x%x]: Render thread completed.\n", std::this_thread::get_id().hash());
	IDMLOG_INFO("[Renderer][0x%x]: Render thread completed.\n", std::hash<std::thread::id>()(std::this_thread::get_id()) );
	
}

//=============================================================================
bool Renderer::InitRender()
{
	IDMLOG_INFO("[Renderer][0x%x]: Init called from thread.\n", std::hash<std::thread::id>()(std::this_thread::get_id()) );

	// quit if it's already initialized
	if (m_init){
		return true;
	}
	
	//if (m_renderThread.get_id().hash() != std::this_thread::get_id().hash())
	if (std::hash<std::thread::id>()(m_renderThread.get_id()) != std::hash<std::thread::id>()(std::this_thread::get_id()))
	{
		IDMLOG_ERROR("Init must occur on render thread (0x%x) but attempted on thread.\n",
					 std::hash<std::thread::id>()(m_renderThread.get_id()),
					 std::hash<std::thread::id>()(std::this_thread::get_id())
		);
		return false;
	}

	// build the context
	m_view = new GLView;

	// initialize glfw
	glfwInit();

    if(tga == m_outputType){
        m_exporter = new IDMImageExporter();
    } else if(yuv == m_outputType) {
        m_exporter = new IDMYUVExporter();
    } else {
        IDMLOG_ERROR("Unknown output type selected (value = %d). Defaulting to yuv.\n", (int)m_outputType);
        m_exporter = new IDMYUVExporter();
    }


    if(!m_pipeName.empty()){
        m_exporter->Open(m_pipeName.c_str(), m_isSequence);
    } else {
        m_exporter->Open("out/out.yuv", m_isSequence);
    }

	m_exporter->SetSize(1, 1);
	m_exporter->Create();

	m_init = true;
	return true;
}

//=============================================================================
void Renderer::CleanUpRender()
{
	//IDMLOG_INFO("[Renderer][0x%x]: CleanUpRender called from thread.\n", std::this_thread::get_id().hash());
	IDMLOG_INFO("[Renderer][0x%x]: CleanUpRender called from thread.\n",
				std::hash<std::thread::id>()(std::this_thread::get_id()) );

	// check that we're doing this on the render thread
	//if (m_renderThread.get_id().hash() != std::this_thread::get_id().hash())
	if (std::hash<std::thread::id>()(m_renderThread.get_id()) != std::hash<std::thread::id>()(std::this_thread::get_id()))
	{
		IDMLOG_ERROR("CleanUpRender must occur on render thread (0x%x) but attempted on thread.\n",
			//m_renderThread.get_id().hash(), std::this_thread::get_id().hash());
					 std::hash<std::thread::id>()(m_renderThread.get_id()),
					 std::hash<std::thread::id>()(std::this_thread::get_id()));

		return;
	}

	m_init = false;

	if (nullptr != m_loader){
		delete m_loader;
		m_loader = nullptr;
	}

	if (m_exporter){
		delete m_exporter;
		m_exporter = nullptr;
	}

	if (m_view){
		delete m_view;
		m_view = nullptr;
	}
}

//=============================================================================
bool Renderer::LoadIdm(const char* idmFile)
{
	// don't run if it's not initialized
	if (!m_init){
		return false;
	}

	// kill if we have one already
	if (nullptr != m_loader){
		UnloadIdm();
	}

	// create one if we don't have one already
	if (nullptr == m_loader){
		m_loader = new IDMLoader;
	}

	// load the file
	IDMResult res = m_loader->Load(idmFile);
	if (res != IDMResult::Ok){
		IDMLOG_ERROR("Could not load idm file %s.\n", idmFile);
		delete m_loader;
		m_loader = nullptr;
		return false;
	}

	// update the description
	m_desc = IdmDesc(m_loader->GetMainScene()->GetWidth(), 
		m_loader->GetMainScene()->GetHeight(), 
		m_loader->GetMainScene()->GetNumFrames());
	IDMLOG_INFO(
		"[Renderer][INFO]: Loaded file %s\n"
		"                   Size: (%d, %d)\n" 
		"                   Frames: %d\n",
		idmFile, m_desc.m_width, m_desc.m_height, m_desc.m_frameCount);

	// update exporter
	UpdateExporter();

	return true;
}

//=============================================================================
void Renderer::UnloadIdm()
{
	// don't run if it's not initialized
	if (!m_init){
		return;
	}

	// only way to unload, currently
	if (m_loader){
		delete m_loader;
		m_loader = nullptr;
	}

	// reset the description
	m_desc = IdmDesc();
}

//=============================================================================
int Renderer::UpdateFrameId()
{
	int newFrameId;
	switch (m_state){
	case PlaybackState::ePSPaused:
		newFrameId = GetFrameId();
		break;
	case PlaybackState::ePSPlay:
		newFrameId = IncrementFrameId();
		break;
	case PlaybackState::ePSPlayReverse:
		newFrameId = DecrementFrameId();
		break;
	}
	return newFrameId;
}

//=============================================================================
int Renderer::DecrementFrameId()
{
	int frameId;
	std::unique_lock<std::mutex> lk(m_mutex);
	--m_frameId;
	if (m_frameId < 0){
		m_frameId += m_desc.m_frameCount;
	}
	frameId = m_frameId;
	lk.unlock();
	return frameId;
}

//=============================================================================
int Renderer::IncrementFrameId()
{
	int frameId;
	std::unique_lock<std::mutex> lk(m_mutex);
	m_frameId = (++m_frameId) % m_desc.m_frameCount;
	frameId = m_frameId;
	lk.unlock();
	return frameId;
}

//=============================================================================
void Renderer::SetFrameId(int frameId)
{
	std::lock_guard<std::mutex> lk(m_mutex);
	m_frameId = frameId;
}

//=============================================================================
int Renderer::GetFrameId() const
{
	int frameId;
	std::unique_lock<std::mutex> lk(m_mutex);
	frameId = m_frameId;
	lk.unlock();
	return frameId;
}

//=============================================================================
void Renderer::SetState(PlaybackState state)
{
	std::lock_guard<std::mutex> lk(m_mutex);
	m_state = state;
}

//=============================================================================
PlaybackState Renderer::GetState() const
{
	PlaybackState state;
	std::unique_lock<std::mutex> lk(m_mutex);
	state = m_state;
	lk.unlock();
	return state;
}


//=============================================================================
void Renderer::SetFps(float fps)
{
	std::lock_guard<std::mutex> lk(m_mutex);
	m_fps = fps;
}

//=============================================================================
float Renderer::GetFps() const
{
	float fps;
	std::unique_lock<std::mutex> lk(m_mutex);
	fps = m_fps;
	lk.unlock();
	return fps;
}

//=============================================================================
bool Renderer::SetAsset(uint32_t id, const char* assetRef)
{
	// check for null
	if (nullptr == m_loader){
		return false;
	}

	IDMLoader::TypedData typedData = m_loader->GetDataById(id);
	if (!IsSprite(typedData.m_type)){
		return false;
	}

	idm::IDMSprite* sprite = reinterpret_cast<idm::IDMSprite*>(typedData.m_data);
	switch (typedData.m_type){
		case IDMLoader::IDMTag::IDMTag_LayerImage:
		{
			idm::IDMImage* img = new idm::IDMImage;
			if (IDMResult::Ok == img->Load(assetRef)){

			}
			img->Create();
			sprite->SetDiffuseTexture(img);
			m_images.push_back(img);
			break;
		}
		case IDMLoader::IDMTag::IDMTag_LayerText:
		{
			std::string strUtf8(assetRef);
			idm::IDMText* txt = (idm::IDMText*) sprite->GetGraphics();
			txt->Clear();
			std::wstring* wstr = new std::wstring(StringUtils::utf82ws(strUtf8));
			txt->DrawText(wstr->c_str());
			m_texts.push_back(wstr);
			break;
		}
		//case IDMLoader::IDMTag::IDMTag_LayerSolid:
		//case IDMLoader::IDMTag::IDMTag_LayerVideo:
		//case IDMLoader::IDMTag::IDMTag_LayerComposition:
	}

	//idm::GLTexture* glTex = sprite->GetDiffuseTexture();
	//glTex->Delete();
	//delete glTex;

	//idm::GLTexture* img2 = sprite->GetDiffuseTexture();

	return true;
}

//=============================================================================
bool Renderer::IsSprite(idm::IDMLoader::IDMTag tag)
{
	if ((IDMLoader::IDMTag::IDMTag_LayerImage == tag) ||
		(IDMLoader::IDMTag::IDMTag_LayerSolid == tag) ||
		(IDMLoader::IDMTag::IDMTag_LayerVideo == tag) ||
		(IDMLoader::IDMTag::IDMTag_LayerComposition == tag) ||
		(IDMLoader::IDMTag::IDMTag_LayerText == tag)){
		return true;
	}
	return false;
}

//=============================================================================
bool Renderer::SubmitCommand(Command* command)
{
	m_cmdQueue.Push(command);
	return true;
}

//=============================================================================
bool Renderer::GetNextDoneRequest(DoneRequest& nextDone)
{
	return m_doneQueue.TryPop(nextDone);
}

//=============================================================================
void Renderer::UpdateExporter()
{
	if (nullptr == m_exporter){
		return;
	}

	if ((m_desc.m_width != m_exporter->GetWidth()) || 
		(m_desc.m_height != m_exporter->GetHeight()))
	{
		m_exporter->Delete();
		m_exporter->SetSize(m_desc.m_width, m_desc.m_height);
		m_exporter->Create();
	}
}

//=============================================================================
void Renderer::Worker()
{
	// wait until the thread is properly set up
	while (!m_ready){
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	// intialize. must occur on this thread
	if (!InitRender()){
		return;
	}

	do
	{
		//if (!LoadIdm("assets/MaskFeatAnimAll.idm")){
		//	break;
		//}

		int count = 0;
		m_state = PlaybackState::ePSPlay;
		m_timer.start();
		m_timerActual.start();
		while (!m_done)
		{
			// process render-thread commands:
			//	1. change TGA/YUV export
			//	2. load new idm
			//  3. update asset
			while (!m_cmdQueue.Empty()){
				Command* cmd = nullptr;
				if (!m_cmdQueue.TryPop(cmd)){
					// error
					continue;
				}

				if ((*cmd)()) {
                    IDMLOG_INFO("[Renderer]: %s() command executed before rendered frame %d: SUCCEEDED \n", idm::cv::RenderCmdStrMap[cmd->m_cmdType].str, count);
				} else {
                    IDMLOG_INFO("[Renderer]: %s() command executed before rendered frame %d: FAILED \n", idm::cv::RenderCmdStrMap[cmd->m_cmdType].str, count);
                }
				delete cmd;
				cmd = nullptr;
			}

			// skip if nothing to render
			if (nullptr == m_loader){
				// nothing to render
				//printf("[Renderer]: Nothing to render!\n");
				std::this_thread::sleep_for(std::chrono::milliseconds(10));
				continue;
			}

			// get current frame
			int frameId = GetFrameId();
			m_loader->GetMainScene()->SetFrame(frameId);

			// render 
			m_loader->GetMainScene()->Render();

			// throttle the frame time - wait the remainder of 1/fps s
			m_timer.stop();
			
			if (m_fps > 0.0)
			{
				// wait the remainder of the expected frame time and the elapsed time
				double elapsedMs = m_timer.getElapsedTimeInMilliSec();
				float msPerFrame = (float) ((1.0 / m_fps) * 1000.0);
				long long waitTime = msPerFrame - elapsedMs;
				if (waitTime > 0){
					std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));
				}
			}
			m_timerActual.stop();
			double actualTimeElapsedMs = m_timerActual.getElapsedTimeInMilliSec();
			IDMLOG_INFO("[Renderer]: Exporting Frame %d of current idm after %d ms. Total rendered: %d \n", frameId, (long)actualTimeElapsedMs, count);
			m_timerActual.start();
			m_timer.start();


			// export
			m_exporter->Export(m_loader->GetMainScene());
			++count;

			UpdateFrameId();
		}
		//IDMLOG_INFO("[Renderer]: [0x%x]: Rendered %d frames.\n", std::this_thread::get_id().hash(), count);
		IDMLOG_INFO("[Renderer]: [0x%x]: Rendered %d frames.\n",
					std::hash<std::thread::id>()(std::this_thread::get_id()), count);


		m_exporter->Close();
		m_exporter->Delete();
		idm::IdmTextManager::Dispose();
		for (IDMImage * obj : m_images) delete obj;
		for (std::wstring * obj : m_texts) delete obj;
	} 
	while (false);


	CleanUpRender();
}