#pragma once

#include "IDMEngine.h"
#include "gl/GLView.h"
#include "gl/GLDebugOutput.h"
#include "IdmTextManager.h"
#include "resources/idm/IDMLoader.h"
#include "exporters/IDMImageExporter.h"
#include "exporters/IDMYUVExporter.h"
#include "scenegraph/IDMScene.h"
#include <GLFW/glfw3.h>
class IDMTinyMS
{
public:

	enum Export { TGA, YUV };

	IDMTinyMS(const char * filename, Export output)
	{
		using namespace idm;

		// create offscren context.
		GLView view;

		glfwInit();

		// load new project.
		m_loading = glfwGetTime();
		IDMLoader idmFile(filename);
		m_loading = glfwGetTime() - m_loading;
		
		int width = idmFile.GetMainScene()->GetWidth();
		int height = idmFile.GetMainScene()->GetHeight();
		int frame = 0;

		m_rendering = glfwGetTime();
		m_totalFrames = idmFile.GetMainScene()->GetNumFrames();

		// setup the exporter.
		IDMExporter * exporter;

		if (output == YUV) {
			exporter = new IDMYUVExporter();
		}
		else {
			exporter = new IDMImageExporter();
		}

		exporter->Open("out/");
		exporter->SetSize(width, height);
		exporter->Create();

		while (frame < idmFile.GetMainScene()->GetNumFrames()) {
			// set current frame.
			idmFile.GetMainScene()->SetFrame(frame++);
			// render the thing!.
			idmFile.GetMainScene()->Render();
			// export current frame.
			exporter->Export(idmFile.GetMainScene());
		}

		// we need to close the exporter after rendering.
		// this will make sure all frames are complete and
		// finish executing all pending threads.
		exporter->Close();

		delete exporter;

		m_rendering = glfwGetTime() - m_rendering;

		idm::IdmTextManager::Dispose();
	}

	double GetLoadingTime() { return m_loading; }

	double GetRenderingTime() { return m_rendering; }

	int GetTotalFrames() { return m_totalFrames; }

private:

	double m_loading;

	double m_rendering;

	int m_totalFrames;
};