#ifndef IMS_ACTOR_ZEROMQ_H
#define IMS_ACTOR_ZEROMQ_H
#pragma once

#include "Actors/IMSActor.h"
#include "print/IMSPrint.h"
#include <map>

namespace zmq
{
    class context_t;
    class socket_t;
    class message_t;
}

namespace idm
{
    namespace ims
    {
        class IMSActorZeroMQ : public IMSActor
        {
        public:
            IMSActorZeroMQ() { m_actorName = "IMSActorZeroMQ"; };
            ~IMSActorZeroMQ() {};

            /*!
            * \brief Process the job
            *
            * \param[in] id - thread id. Internal use of the thread pool
            * \param[in] job - job to work on
            */
            virtual void ProcessJob(int id, IMSJob* job) override;

            /*!
            * \brief Start Actor execution (all internal threads are executed)
            * \return true on success false otherwise
            */
            virtual bool Start() override;


            /*!
            * \brief Stops Actor execution. The Actor will wait for all processed Jobs to finish
            *        and only then will stop the threads
            * \return true on success false otherwise
            */
            virtual bool Stop() override;

        private:
            
            bool Connect();
            void Disconnect();

            void ZMQProtocolListener();

            bool ParseInputMessage(const std::string& strMsg, std::vector<IMSJob*>& jobsList);
            

            struct SocketInfo
            {
                /// default constructor
                SocketInfo() : m_socket(nullptr), m_addr(""), m_type(-1), m_bindOrConnect(false) { }

                zmq::socket_t*		m_socket;           // read/write socket
                std::string			m_addr;             // ZeroMQ-style connecting/binding address
                int					m_type;             // ZeroMQ socket type 
                bool				m_bindOrConnect;    // Whether to bind (true) or connect (false) to the socket
            };

            SocketInfo			m_inputSocketInfo;
            SocketInfo			m_outputSocketInfo;

            zmq::context_t*     m_zmqContext;

            std::thread         m_listenerThread;
            IIMSActor*          m_dispatcher;

            tscq::ConcurrentQueue<std::string> m_resultsQueue;
            
            void SendOputputMessages();
            void ZMQMessage2String(const zmq::message_t& zmqMessage, std::string& message);
        };
    }
}

#endif // IMS_ACTOR_ZEROMQ_H