#include "IMSActor.h"
#include <mutex>
#include "objectpool\IMSObjectPool.h"


namespace idm
{
    namespace ims
    {

        IMSActor::IMSActor() 
        {
            Init(1);
        }

        IMSActor::IMSActor(uint32_t numThreads)
        {
            Init(numThreads);
        }

        IMSActor::~IMSActor() {}


        void IMSActor::Reset()
        {
            Stop();
            Start();
        }
        
        void IMSActor::Init(uint32_t numThreads)
        {
            IMSObjectPool& objectPool = IMSObjectPool::GetInstance();
            m_console = objectPool.GetConsole();
        
            m_numThreads = numThreads;
        }

        bool IMSActor::Start()
        {
            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // No need to start already started thread, otherwise the old threads will be unhandled
            if (m_isStartted) {
                return true;
            }

            m_isStartted = true;
            m_isStopped = false;

            // Prepare the memory pool
            m_workerThreadPool.restart(m_numThreads);

            // Create Actor's threads
            m_readyQueueProcessThread = std::thread(&IMSActor::JobEndProcess, this);

          
            return true;
        }

        bool IMSActor::Stop()
        {
            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // If actor was already stopped, return
            if (m_isStopped) {
                return true;
            }

            m_isStopped = true;
            m_isStartted = false;

    

            // Remove all Jobs from the queue
            // Note! the job that are currently in Actor's queues 
            //       will return to the caller, however, the jobs currently
            //       processed will be dropped. 
            //
            //       The premise: we don't care about the Jobs on stop
            ///      as we shutting down. 

            // The processed job will finish, but waiting threads will be dropped
            m_workerThreadPool.stop(false);

            IMSJob* job = nullptr;
            while (m_readyQueue.try_dequeue(job))    {job->SetRenderState(JobRenderState_Stop); }

            // Notify all Actors thread
            m_readyQueueChange.notify_all();

            // Wait for the Actors thread to complete
            if (m_readyQueueProcessThread.joinable()) {
                m_readyQueueProcessThread.join();
            }

            return true;
        }

        bool IMSActor::Submit(IMSJob* job)
        {
            if (!job || m_isStopped) {
                return false;
            }

            // Store pointer to the Actor
            job->CurrentActor = this;

            m_workerThreadPool.push([this](int id, IMSJob* job){ ProcessJob(id, job); }, job);

            return true;
        }


        void IMSActor::ProcessJob(int id, IMSJob* job)
        {
            job->SetRenderState(JobRenderState_Stop);
            Done(job);
        }



        void IMSActor::Done(IMSJob* job)
        {
            m_readyQueue.enqueue(job);
            m_readyQueueChange.notify_all();
        }

        void IMSActor::JobEndProcess()
        {
            std::unique_lock<std::mutex> lk(m_readyQueueMutex);

            while (!m_isStopped)
            {
                // Wait for the change in the waiting queue or for the stop flag
                m_readyQueueChange.wait(lk, [this]() {return ((m_readyQueue.size_approx() > 0) || m_isStopped); });

                IMSJob* job = nullptr;

                if (m_isStopped) { 
                    // Dequeue all jobs from the ready queue
                    // (in case where added by processing workers after
                    //  the cleanup in Stop function)
                    while (m_readyQueue.try_dequeue(job)) {};
                    return; 
                }

                // Complete all ready jobs
                while (m_readyQueue.size_approx() > 0)
                {
                    if (!m_readyQueue.try_dequeue(job)) {
                        // no more jobs in the queue or sporadic wakeup
                        continue;
                    }

                    // Return the job to the caller
                    if (job->CallerActor) {
                        job->CallerActor->Submit(job);
                    }
                }
            }
        }


    }
}