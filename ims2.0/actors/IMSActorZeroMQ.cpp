#include "IMSActorZeroMQ.h"
#include <zmq.hpp>
#include "print/IMSPrint.h"
#include "objectpool/IMSObjectPool.h"
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include "global/IMSErrors.h"
#include "utils/IMSCommandParser.h"


namespace idm {
    namespace ims {

        static const char* ZmqSockTypeNames[] =
        {
            "ZMQ_PAIR",
            "ZMQ_PUB",
            "ZMQ_SUB",
            "ZMQ_REQ",
            "ZMQ_REP",
            "ZMQ_DEALER",
            "ZMQ_ROUTER",
            "ZMQ_PULL",
            "ZMQ_PUSH",
            "ZMQ_XPUB",
            "ZMQ_XSUB",
            "ZMQ_STREAM"
        };


        //----------------------------------------------------------------------------------------------------------------
        std::string GetJsonText(const rapidjson::Value& value)
        {
            rapidjson::StringBuffer buffer;

            buffer.Clear();

            rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
            value.Accept(writer);

            return std::string(buffer.GetString());
        }


        inline std::string BuildJsonError(uint64_t errorCode, const std::string& errorMsg)
        {
            rapidjson::Document jdocError;
            rapidjson::Value jvalErrorMsg;

            jvalErrorMsg.SetString(errorMsg.c_str(), (rapidjson::SizeType)errorMsg.length(), jdocError.GetAllocator());

            jdocError.AddMember(IDM_REQUEST_TAG_ERROR_CODE, errorCode, jdocError.GetAllocator());
            jdocError.AddMember(IDM_REQUEST_TAG_MESSAGE, jvalErrorMsg, jdocError.GetAllocator());

            return GetJsonText(jdocError);
        }

        void IMSActorZeroMQ::ProcessJob(int id, IMSJob* job)
        {
            IMS_TRACE(m_console, "Job (%d) accepted at actor (%s)\n", job->GetJobID(), m_actorName.c_str());

            IMSJobReport& report = job->Report;

            double msPerFrame = (report.m_framesRendered == 0) ? 0 : (report.m_renderTimeMs / report.m_framesRendered);
            double fps = (report.m_framesRendered == 0) ? 0 : (1000.0 / msPerFrame);


            // See if there's user data in the headers
            std::string userData;
            rapidjson::Document docUserData;
            HeaderMap::const_iterator iter = job->RequestHeaders.find(IDM_REQUEST_TAG_USER_DATA);
            if (iter != job->RequestHeaders.end()) {

                userData = iter->second;
                if (docUserData.Parse(userData.c_str()).HasParseError())
                {
                    rapidjson::Value empty(rapidjson::kObjectType);
                    docUserData.Clear();
                }
            }

            // Builds JSON report
            rapidjson::Document root;
            root.SetObject();
            rapidjson::Document::AllocatorType& allocator = root.GetAllocator();
            rapidjson::Value sValue;

            // Convert message to JSON string value
            sValue.SetString(report.m_message.c_str(), (rapidjson::SizeType)report.m_message.length(), allocator);

            root.AddMember(IDM_REQUEST_TAG_REQID, report.m_id, allocator);
            root.AddMember(IDM_RENDERREQUEST_TAG_RENDER_TIME, report.m_renderTimeMs, allocator);
            root.AddMember(IDM_RENDERREQUEST_TAG_FRAME_COUNT, report.m_framesRendered, allocator);
            root.AddMember(IDM_RENDERREQUEST_TAG_FPS, fps, allocator);
            root.AddMember(IDM_REQUEST_TAG_ERROR_CODE, report.m_errorCode, allocator);
            root.AddMember(IDM_RENDERREQUEST_TAG_ERROR_SCENEID, report.m_errorSceneId, allocator);
            root.AddMember(IDM_REQUEST_TAG_MESSAGE, sValue, allocator);
            if (!docUserData.IsNull()){
                root.AddMember(IDM_REQUEST_TAG_USER_DATA, docUserData, allocator);
            }

            // serialize to a string and send it off
            std::string strRoot = GetJsonText(root);

            m_resultsQueue.enqueue(strRoot);

            //	Dump Job status to the console
            IMS_TRACE(m_console, "[ZeroMQ]: FinishingJob() \n");
            IMS_TRACE(m_console, "  %s:\t%"PRId64"\n", IDM_REQUEST_TAG_REQID, report.m_id);
            IMS_TRACE(m_console, "  %s:\t%f\n", IDM_RENDERREQUEST_TAG_RENDER_TIME, report.m_renderTimeMs);
            IMS_TRACE(m_console, "  %s:\t%"PRId32"\n", IDM_RENDERREQUEST_TAG_FRAME_COUNT, report.m_framesRendered);
            IMS_TRACE(m_console, "  %s:\t\t%f (%f ms / frame)\n", IDM_RENDERREQUEST_TAG_FPS, fps, msPerFrame);
            IMS_TRACE(m_console, "  %s:\t%"PRId32" (%s)\n", IDM_REQUEST_TAG_ERROR_CODE, report.m_errorCode, IMSErrorMappingTable[report.m_errorCode].Message);

            if (report.m_errorCode != ErrorCode_NoErrors) {
                m_console->SetColor(ConsoleColor_Red);
            }

            IMS_TRACE(m_console, "  %s:\t%"PRId32"\n", IDM_RENDERREQUEST_TAG_ERROR_SCENEID, report.m_errorSceneId);
            IMS_TRACE(m_console, "  %s:\t%s\n", IDM_REQUEST_TAG_MESSAGE, report.m_message.c_str());
            if (report.m_errorCode != ErrorCode_NoErrors) {
                m_console->SetColor(ConsoleColor_White);
            }

            if (!userData.empty()){
                IMS_TRACE(m_console, "  %s:\t\n%s\n", IDM_REQUEST_TAG_USER_DATA, userData.c_str());
            }
            IMS_TRACE(m_console, "\n");


            // Remove the Job
            m_objectPool.DeleteJob(job);

            // -- Job destroyed. We shouldn't call the Done function
            // Done(job);
        }

        //----------------------------------------------------------------------------------------------------------------

        bool IMSActorZeroMQ::Start()
        {

            // We override a default Start function as we want to add some functionality 
            // to ZeroMQ vector. 
            // We create additional thread for listening
            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // No need to start already started thread, otherwise the old threads will be unhandled
            if (m_isStartted) {
                return true;
            }

            m_isStartted = true;
            m_isStopped = false;

            // Prepare the memory pool
            m_workerThreadPool.restart(m_numThreads);

            if (!Connect()) {
                return false;
            }

            m_listenerThread = std::thread(&IMSActorZeroMQ::ZMQProtocolListener, this);

            m_dispatcher = m_objectPool.GetActor(ActorType_Dispatcher);
            
            return true;
        }


        bool IMSActorZeroMQ::Stop()
        {
            // We override a default Stop function as we want to add some functionality 
            // to ZeroMQ vector. 
            // We want to stop an addition thread we created

            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // If actor was already stopped, return
            if (m_isStopped) {
                return true;
            }

            m_isStopped = true;
            m_isStartted = false;


            // The processed job will finish, but waiting threads will be dropped
            m_workerThreadPool.stop(false);

            if (m_listenerThread.joinable()) {
                m_listenerThread.join();
            }

            return true;
        }

        bool IMSActorZeroMQ::ParseInputMessage(const std::string& strMsg, std::vector<IMSJob*>& jobsList)
        {
            jobsList.clear();

            rapidjson::Document root;
            if (root.Parse(strMsg.c_str()).HasParseError())
            {
                IMS_ERROR_FULL(m_console, "[ZeroMQ]: Unparsable request.\n");
                m_resultsQueue.enqueue(BuildJsonError(ErrorCode_CorruptMessage, "Unparsable request."));
                return false;
            }


            // Parse out the input message: the message should include the below data:
            //      - request_id
            //      - cmd 
            //      - userdata
            //      - args


            // Required parameter: request_id
            if (!root.HasMember(IDM_REQUEST_TAG_REQID) || !root[IDM_REQUEST_TAG_REQID].IsInt64())
            {
                IMS_ERROR_FULL(m_console, "[ZeroMQ]: Invalid command: request_id not present or malformed.\n");
                m_resultsQueue.enqueue(BuildJsonError(ErrorCode_InvalidCommand, "Invalid command: request_id not present or malformed."));
                return false;
            }

            // Required parameter: cmd
            if (!root.HasMember(IDM_REQUEST_TAG_CMD) || !root[IDM_REQUEST_TAG_CMD].IsString())
            {
                IMS_ERROR_FULL(m_console, "[ZeroMQ]: Invalid command: cmd not a string.\n");
                m_resultsQueue.enqueue(BuildJsonError(ErrorCode_InvalidCommand, "Invalid command: cmd not present or malformed"));
                return false;
            }

            uint64_t requestId = root[IDM_REQUEST_TAG_REQID].GetUint64();
            std::string command = root[IDM_REQUEST_TAG_CMD].GetString();


            // Optional parameter: args (must be string)
            std::string payload("");
            if (root.HasMember(IDM_REQUEST_TAG_ARG)) {

                if (!root[IDM_REQUEST_TAG_ARG].IsString()) {
                    IMS_ERROR(m_console, "[ZeroMQ]: Invalid command: arg (payload) not a string or malformed.\n");
                    m_resultsQueue.enqueue(BuildJsonError(ErrorCode_InvalidCommand, "Invalid command: arg not a string or malformed."));
                    return false;
                }

                payload = root[IDM_REQUEST_TAG_ARG].GetString();
            }


            // Optional parameter: userdata (must be a json object)
            //                     Future optional parameters are expected to be part of Headers objects
            HeaderMap headers;
            if (root.HasMember(IDM_REQUEST_TAG_USER_DATA)) {
                rapidjson::Value& valUserData = root[IDM_REQUEST_TAG_USER_DATA];

                if (!valUserData.IsObject()){
                    IMS_ERROR(m_console, "[ZeroMQ]: Invalid command: user_data not a json object or malformed.\n");
                    m_resultsQueue.enqueue(BuildJsonError(ErrorCode_InvalidCommand, "Invalid command: userData not a JSON object or malformed."));
                    return false;
                }

                headers[IDM_REQUEST_TAG_USER_DATA] = GetJsonText(valUserData);
            }


            EErrorCode errorCode;
            std::string errorMessage;
            if (!IMSCommandParser::CreateIMSJobsList(requestId, command, payload, headers, jobsList, errorCode, errorMessage))
            {
                IMS_ERROR(m_console, "[ZeroMQ]: Failed to parse user request.\n");
                m_resultsQueue.enqueue(BuildJsonError(errorCode, errorMessage.c_str()));
                return false;
            }

            // Command was parsed successfully. Jobs list created
            return false;
        }



        //----------------------------------------------------------------------------------------------------------------

        bool IMSActorZeroMQ::Connect()
{
            m_zmqContext = new zmq::context_t(1);

            // Initialize and construct input and output sockets 
            int socketOptionType;
            size_t socketOptionLength = sizeof(int);


            // Initialize input output socket data
            IMSConfig* config = m_objectPool.GetConfig();
            m_inputSocketInfo.m_addr = config->ZMQ.JobsSockAddr;
            m_inputSocketInfo.m_bindOrConnect = config->ZMQ.JobsSockBindConnect;
            m_inputSocketInfo.m_type = config->ZMQ.JobsSockType;

            m_outputSocketInfo.m_addr = config->ZMQ.ResultsSockAddr;
            m_outputSocketInfo.m_bindOrConnect = config->ZMQ.ResultsSockBindConnect;
            m_outputSocketInfo.m_type = config->ZMQ.ResultsSockType;

            IMS_TRACE(m_console, "     Input socket address:    \t\t%s\n",       m_inputSocketInfo.m_addr.c_str());
            IMS_TRACE(m_console, "     Input socket type: \t\t\t%d (%s)\n",      m_inputSocketInfo.m_type, ZmqSockTypeNames[m_inputSocketInfo.m_type]);
            IMS_TRACE(m_console, "     Input socket bind/connect: \t\t%s\n",    (m_inputSocketInfo.m_bindOrConnect ? "bind()" : "connect()"));

            IMS_TRACE(m_console, "     Output socket address:    \t\t%s\n",    m_outputSocketInfo.m_addr.c_str());
            IMS_TRACE(m_console, "     Output socket type: \t\t\t%d (%s)\n",   m_outputSocketInfo.m_type, ZmqSockTypeNames[m_outputSocketInfo.m_type]);
            IMS_TRACE(m_console, "     Output socket bind/connect: \t\t%s\n", (m_outputSocketInfo.m_bindOrConnect ? "bind()" : "connect()"));
            IMS_TRACE(m_console, "     ZeroMQ listener initialized \n");



            m_inputSocketInfo.m_socket = new zmq::socket_t(*m_zmqContext, m_inputSocketInfo.m_type);
            m_inputSocketInfo.m_socket->getsockopt(ZMQ_TYPE, (void*)&socketOptionType, &socketOptionLength);
            IMS_INFO(m_console, "[ZeroMQ]: Created input socket with type %d (%s)\n", socketOptionType, ZmqSockTypeNames[socketOptionType]);


            m_outputSocketInfo.m_socket = new zmq::socket_t(*m_zmqContext, m_outputSocketInfo.m_type);
            m_outputSocketInfo.m_socket->getsockopt(ZMQ_TYPE, (void*)&socketOptionType, &socketOptionLength);
            IMS_INFO(m_console, "[ZeroMQ]: Created output socket with type %d (%s)\n", socketOptionType, ZmqSockTypeNames[socketOptionType]);



            // Connect input socket
            try {
                if (m_inputSocketInfo.m_bindOrConnect) {
                    IMS_INFO(m_console, "[ZeroMQ]: Calling bind() on input socket at address %s\n", m_inputSocketInfo.m_addr.c_str());
                    m_inputSocketInfo.m_socket->bind(m_inputSocketInfo.m_addr.c_str());
                }
                else {
                    IMS_INFO(m_console, "[ZeroMQ]: Calling connect() on input socket at address %s\n", m_inputSocketInfo.m_addr.c_str());
                    m_inputSocketInfo.m_socket->connect(m_inputSocketInfo.m_addr.c_str());
                }
            }
            catch (const zmq::error_t& e){
                IMS_ERROR(m_console, "[ZeroMQ]: Error connecting/binding to input socket (message: %s)\n", e.what());
                Disconnect();
                return false;
            }

            // Connect output socket
            try {
                if (m_outputSocketInfo.m_bindOrConnect){
                    IMS_INFO(m_console, "[ZeroMQ]: Calling bind() on output socket at address %s\n", m_outputSocketInfo.m_addr.c_str());
                    m_outputSocketInfo.m_socket->bind(m_outputSocketInfo.m_addr.c_str());
                }
                else {
                    IMS_INFO(m_console, "[ZeroMQ]: Calling connect() on output socket at address %s\n", m_outputSocketInfo.m_addr.c_str());
                    m_outputSocketInfo.m_socket->connect(m_outputSocketInfo.m_addr.c_str());
                }
            }
            catch (const zmq::error_t& e){
                IMS_ERROR(m_console, "[ZeroMQ]: Error connecting/binding to output socket (message: %s)\n", e.what());
                Disconnect();
                return false;
            }

            return true;

        }

        void IMSActorZeroMQ::Disconnect()
        {
            if (m_inputSocketInfo.m_socket){
                delete m_inputSocketInfo.m_socket;
                m_inputSocketInfo.m_socket = nullptr;
            }

            if (m_outputSocketInfo.m_socket){
                delete m_outputSocketInfo.m_socket;
                m_outputSocketInfo.m_socket = nullptr;
            }
        }

        //----------------------------------------------------------------------------------------------------------------
        void IMSActorZeroMQ::ZMQProtocolListener()
        {


            std::vector<IMSJob*> jobsList;
            std::string message;

            while (!m_isStopped)
            {
                // check sockets are ok
                if (!m_inputSocketInfo.m_socket) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                    continue;
                }

                // This part doesn't belong to listener, but instead of
                // managing another thread we use the current "always active"
                // worker to dispatch results to the caller. 
                SendOputputMessages();

                // try to receive a message. don't wait.
                zmq::message_t zmqMessage;
                bool recv_result = m_inputSocketInfo.m_socket->recv(&zmqMessage, ZMQ_DONTWAIT);
                if (!recv_result)
                {
                    // recv returning false means zmq_errno () == EAGAIN, meaning no messages are available

                    // sleep for a ms so we're not spinning the CPU
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                    continue;
                }

                
                ZMQMessage2String(zmqMessage, message);

                // Convert the message to a job list
                ParseInputMessage(message, jobsList);


                for (auto job : jobsList) {
                    job->DestroyActor = this;
                    m_dispatcher->Submit(job);
                }
                jobsList.clear();
            }

            IMS_INFO(m_console, "[ZeroMQ]: Listener thread ending...\n");
            Disconnect();
        }

        void IMSActorZeroMQ::SendOputputMessages()
        {
            std::string resultMessage;
            while (m_resultsQueue.try_dequeue(resultMessage))
            {
                if (resultMessage.empty()) 
                        { continue; }

                zmq::message_t message(resultMessage.c_str(), resultMessage.size());
                m_outputSocketInfo.m_socket->send(message);
            }
        }


        void IMSActorZeroMQ::ZMQMessage2String(const zmq::message_t& zmqMessage, std::string& message)
        {
            message.clear();

            // if we're here, a message is waiting
            size_t msgLen = zmqMessage.size();
            std::vector<char> msg((char*)zmqMessage.data(), (char*)zmqMessage.data() + msgLen + 1);
            msg.back() = '\0';
            message = &msg[0];

            IMS_TRACE(m_console, "[ZeroMQ]: Msg size %d:\n", message.size());
            IMS_TRACE(m_console, "[ZeroMQ]: Msg contents: %s\n", message.c_str());
        }

    }
}