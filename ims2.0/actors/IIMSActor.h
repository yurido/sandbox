#pragma 
#ifndef IIMS_ACTOR_H
#define IIMS_ACTOR_H

#include "job/IMSJob.h"

namespace idm
{
	namespace ims
	{

		/*!
		 * \class IIMSActor
		 *
		 * \brief An interface for the Actor class. It's mainly use to conceal all the concurrency 
         *        primitives from the user and keep him with a clean and simple interface
		 *
		 * \author yurido
		 * \date August 2017
		 */
        class IIMSActor
        {
        public:
            virtual ~IIMSActor() {};

		    /*!
		     * \brief Initialize and configure actor behavior
		     *
		     * \param[in] numThreads - number of working threads of the actor
		     * \return true on success false otherwise
		     */
            virtual void Init(uint32_t numThreads) = 0;


            /*!
            * \brief Resets internal state of the actor, clear queues and etc.
            */
            virtual void Reset() = 0;
		
		    /*!
		     * \brief Starts actor threads
		     * \return true on success false otherwise
		     */
            virtual bool Start() = 0;
		
		
		    /*!
		    * \brief Stops actor threads
		    * \return true on success false otherwise
		    */
            virtual bool Stop() = 0;
		
		    /*!
		     * \brief Submit new Job to the actor
		     *
		     * \param[in] job - new IMS Job
		     * \return true on success false otherwise
		     */
		    virtual bool Submit(IMSJob* job) = 0;


            /*!
             * \brief Process the job
             *
             * \param[in] id - thread id. Internal use of the thread pool
             * \param[in] job - job to work on
             */
            virtual void ProcessJob(int id, IMSJob* job) = 0;
		};
	}
}

#endif // IIMS_ACTOR_H
