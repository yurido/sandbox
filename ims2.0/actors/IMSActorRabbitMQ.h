#ifndef IMS_ACTOR_RABBITMQ_H
#define IMS_ACTOR_RABBITMQ_H
#pragma once
#include "IMSActor.h"

#include "amqp.h"
#include "amqp_tcp_socket.h"
#include "amqp_framing.h"

#include "global/IMSTypes.h"
#include "global/IMSErrors.h"

//#include <utility>




namespace idm {
    namespace ims {

        class IMSActorRabbitMQ :  public IMSActor
        {
        public:
            IMSActorRabbitMQ();
            ~IMSActorRabbitMQ();

            /*!
            * \brief Process the job
            *
            * \param[in] id - thread id. Internal use of the thread pool
            * \param[in] job - job to work on
            */
            virtual void ProcessJob(int id, IMSJob* job) override;

            /*!
            * \brief Start Actor execution (all internal threads are executed)
            * \return true on success false otherwise
            */
            virtual bool Start() override;


            /*!
            * \brief Stops Actor execution. The Actor will wait for all processed Jobs to finish
            *        and only then will stop the threads
            * \return true on success false otherwise
            */
            virtual bool Stop() override;

        private:
            void RMQProtocolListener();

            bool Connect();
            void Disconnect();

            typedef struct response_t {
                uint64_t    requestID;
                EErrorCode  errorCode;
                std::string message;
                HeaderMap   headers;
            } response_t;

            tscq::ConcurrentQueue<response_t> m_resultsQueue;

            std::mutex              m_mutex;
            std::thread             m_listenerThread;
            IIMSActor*              m_dispatcher;

            amqp_connection_state_t m_conn_state;               // state information about the connection
            amqp_socket_t*			m_socket;                   // network socket (ultimately winsock)
            amqp_channel_t			m_channel_id;               // integer "name" we give to a rabbit channel
            amqp_bytes_t			m_consumer_tag;             // a string representing who this consumer is
            amqp_bytes_t			m_render_queue_name;        // a string representing the name of the render queue
            amqp_bytes_t			m_done_queue_name;          // a string representing the name of the done queue

            // RabbitMQ extension API
            std::string amqp_bytes_to_str(const amqp_bytes_t_& key);
            std::string amqp_utf8_field_to_str(amqp_field_value_t val);
            void amqp_clear_and_set_string(amqp_bytes_t& amqpString, const char* str);
            void amqp_bytes_zero(amqp_bytes_t* bytes);
            bool return_on_amqp_error(amqp_rpc_reply_t x, char const *context);
            bool return_on_error(int x, char const *context);

            // Utility functions
            response_t BuildErrorMessage(uint64_t requestID, EErrorCode errorCode, const std::string& message);
            bool ParseRMQHeaders(const amqp_envelope_t& envelope, std::vector<IMSJob*>& jobsList);
        };
    }
}
#endif // IMS_ACTOR_RABBITMQ_H

