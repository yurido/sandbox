#ifndef IMS_ACTOR_DISPATCHER_H
#define IMS_ACTOR_DISPATCHER_H
#pragma once

#include "Actors/IMSActor.h"
#include "print/IMSPrint.h"



namespace idm
{
    namespace ims
    {
        class IMSActorDispatcher : public IMSActor
        {
        public:
            IMSActorDispatcher() 
            { 
                m_actorName = "IMSActorDispatcher";
            };
            ~IMSActorDispatcher() {};

            /*!
            * \brief Process the job
            *
            * \param[in] id - thread id. Internal use of the thread pool
            * \param[in] job - job to work on
            */
            virtual void ProcessJob(int id, IMSJob* job) override
            {
                IMS_TRACE(m_console, "Job (%d) accepted at actor (%s)\n", job->GetJobID(), m_actorName.c_str());


                job->CallerActor = this;

                // Naive implementation, 
                if (job->GetStatus() != JobStatus_Success) {
                    m_finalizeActor->Submit(job);
                }
                else if (job->GetRequestType() == RequestType_ListAssets) {

                }
                else if (job->GetRequestType() == RequestType_ClearAssets) {

                }
                else if (job->GetRequestType() == RequestType_Shutdown) {

                }
                else if (job->GetRequestType() == RequestType_Render) {
                    switch (job->GetRenderState())
                    {
                    case JobRenderState_Create:
                    {
                        EProtocolType protocol = job->GetProtocolType();
                        if (protocol == ProtocolType_ZeroMQ) {
                            m_zeroMQActor->Submit(job);
                        }
                        else if (protocol == ProtocolType_RabitMQ) {

                        }
                        else if (protocol == ProtocolType_RabitMQ) {

                        }
                    }
                        break;

                    case jobRenderState_Init:
                        m_parserActor->Submit(job);
                        break;

                    case jobRenderState_Parse:
                        m_renderActor->Submit(job);
                        break;

                    case JobRenderState_Render:
                        m_finalizeActor->Submit(job);
                        break;

                    case JobRenderState_Stop:
                        m_finalizeActor->Submit(job);
                        break;

                    case JobRenderState_Finalize:
                        //job->DestroyActor->Submit(job);
                        break;


                    default:
                        IMS_ERROR(m_console, "Unsupported render state (%d)", job->GetRequestType());
                        break;
                    }
                }
                else {
                    IMS_ERROR(m_console, "Unsupported request type (%d)", job->GetRequestType());
                }


                // -- Dispatcher, doesn't call to Done
                // -- function, as there is no ReadyQueue in the dispatcher.
                // Done(job);
            }


            virtual bool Start() override
            {
                IMSActor::Start();

                // Initialize references to all Actors in the system
                m_parserActor    = m_objectPool.GetActor(ActorType_Parser);
                m_renderActor    = m_objectPool.GetActor(ActorType_Render);
                m_finalizeActor  = m_objectPool.GetActor(ActorType_Finalize);
                m_commanderActor = m_objectPool.GetActor(ActorType_Commander);
                m_zeroMQActor    = m_objectPool.GetActor(ActorType_ZeroMQ);

                return true;
            }

        private:
            IIMSActor* m_parserActor;
            IIMSActor* m_renderActor;
            IIMSActor* m_finalizeActor;
            IIMSActor* m_commanderActor;
            IIMSActor* m_zeroMQActor;
        };
    }
}

#endif // IMS_ACTOR_DISPATCHER_H