#pragma once
#ifndef IMS_ACTOR_TEST_H
#define IMS_ACTOR_TEST_H

#include "IMSActor.h"

namespace idm
{
    namespace ims
    {
        class IMSActorTest : public IMSActor
        {
        public:
            IMSActorTest() { m_actorName = "IMSActorTest"; };

            /*!
            * \brief Process the job
            *
            * \param[in] id - thread id. Internal use of the thread pool
            * \param[in] job - job to work on
            */
            virtual void ProcessJob(int id, IMSJob* job) override
            {
                IMS_TRACE(m_console, "Job (%d) accepted at actor (%s)\n", job->GetJobID(), m_actorName.c_str());

                // We want to test that each job returned from the actor with
                // new state (JobRenderState_Test) - meaning, the job is processed
                job->SetRenderState(JobRenderState_Test);


                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            }
        };
    }
}

#endif // IMS_ACTOR_TEST_H
