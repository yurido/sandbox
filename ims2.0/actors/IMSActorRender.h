#ifndef IMS_ACTOR_RENDER_H
#define IMS_ACTOR_RENDER_H
#pragma once

#include "Actors/IMSActor.h"
#include "print/IMSPrint.h"



namespace idm
{
    namespace ims
    {
        class IMSActorRender : public IMSActor
        {
        public:
            IMSActorRender() { m_actorName = "IMSActorRender"; };
            ~IMSActorRender() {};

            /*!
            * \brief Process the job
            *
            * \param[in] id - thread id. Internal use of the thread pool
            * \param[in] job - job to work on
            */
            virtual void ProcessJob(int id, IMSJob* job) override
            {
                IMS_TRACE(m_console, "Job (%d) accepted at actor (%s)\n", job->GetJobID(), m_actorName.c_str());

                // We want to test that each job returned from the actor with
                // new state (JobRenderState_Test) - meaning, the job is processed
                job->SetRenderState(JobRenderState_Render);


                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            }

        private:

        };
    }
}

#endif // IMS_ACTOR_RENDER_H