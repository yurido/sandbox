#include "IMSActorRabbitMQ.h"

#include "amqp_tcp_socket.h"
#include "amqp_framing.h"
#include "amqp.h"

#include "global/IMSConfig.h"
#include "global/IMSErrors.h"
#include "utils/IMSCommandParser.h"
#include "print/IMSPrint.h"
#include "job/IMSJob.h"

#include "utils/StringUtils.h"
#include <mutex>


#ifdef _WIN32
#include <Windows.h>
#include "winsock.h"
#endif
#include <algorithm>
#include <vector>



namespace idm {
    namespace ims {

        #define IDM_LISTENER_THREAD_CONSUME_TIMEOUT_SEC 3

        IMSActorRabbitMQ::IMSActorRabbitMQ()
            : m_conn_state(nullptr)
            , m_socket(nullptr)
            , m_channel_id(1)
            , m_consumer_tag(amqp_cstring_bytes("ip_worker_01"))	// will be built dynamically <ip_address>_worker_<nn>
        {
            m_actorName = "IMSActorRabbitMQ";

            amqp_bytes_zero(&m_render_queue_name);
            amqp_bytes_zero(&m_done_queue_name);
        }


        IMSActorRabbitMQ::~IMSActorRabbitMQ()
        {
        }

        void IMSActorRabbitMQ::ProcessJob(int id, IMSJob* job)
        {

        }

        bool IMSActorRabbitMQ::Start()
        {

            // We override a default Start function as we want to add some functionality 
            // to ZeroMQ vector. 
            // We create additional thread for listening
            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // No need to start already started thread, otherwise the old threads will be unhandled
            if (m_isStartted) {
                return true;
            }

            m_isStartted = true;
            m_isStopped = false;

            // Prepare the memory pool
            m_workerThreadPool.restart(m_numThreads);

            if (!Connect()) {
                return false;
            }

            m_listenerThread = std::thread(&IMSActorRabbitMQ::RMQProtocolListener, this);

            m_dispatcher = m_objectPool.GetActor(ActorType_Dispatcher);

            return true;
        }

        bool IMSActorRabbitMQ::Stop()
        {
            // We override a default Stop function as we want to add some functionality 
            // to ZeroMQ vector. 
            // We want to stop an addition thread we created

            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // If actor was already stopped, return
            if (m_isStopped) {
                return true;
            }

            m_isStopped = true;
            m_isStartted = false;


            // The processed job will finish, but waiting threads will be dropped
            m_workerThreadPool.stop(false);

            if (m_listenerThread.joinable()) {
                m_listenerThread.join();
            }

            return true;
        }

        void IMSActorRabbitMQ::RMQProtocolListener()
        {
            std::vector<IMSJob*> jobsList;
            std::string message;
            timeval timeout_value = { IDM_LISTENER_THREAD_CONSUME_TIMEOUT_SEC, 0 };

            while (!m_isStopped)
            {
                // try to release some memory if possible
                amqp_maybe_release_buffers(m_conn_state);

                amqp_envelope_t envelope;
                amqp_rpc_reply_t res = amqp_consume_message(m_conn_state, &envelope, &timeout_value, 0);

                if (AMQP_RESPONSE_NORMAL != res.reply_type)
                {
                    if (AMQP_RESPONSE_LIBRARY_EXCEPTION == res.reply_type &&
                        AMQP_STATUS_TIMEOUT == res.library_error)
                    {
                        // normal timeout -- give up for a round to check if the main program wants to shut down, etc
                        continue;
                    }
                    else
                    {
                        // all other abnormal responses
                        IMS_WARNING(m_console, "[RabbitMQ]: Reply type (%d) was not a normal response.\n", res.reply_type);
                        amqp_basic_ack(m_conn_state, m_channel_id, envelope.delivery_tag, 0);
                        continue;
                    }
                }


                IMS_INFO(m_console, "[RabbitMQ]: Delivery: [%u] | exchange: [%.*s] | routingkey: [%.*s]\n",
                    (unsigned)envelope.delivery_tag,
                    (int)envelope.exchange.len, (char *)envelope.exchange.bytes,
                    (int)envelope.routing_key.len, (char *)envelope.routing_key.bytes);

                if (envelope.message.properties._flags & AMQP_BASIC_CONTENT_TYPE_FLAG) {
                    IMS_INFO(m_console, "[RabbitMQ]: Content-type: [%.*s]\n",
                        (int)envelope.message.properties.content_type.len,
                        (char *)envelope.message.properties.content_type.bytes);
                }


                HeaderMap headers;
                if (0 == envelope.message.properties.headers.num_entries)
                {
                    m_resultsQueue.enqueue(BuildErrorMessage(envelope.delivery_tag, ErrorCode_InvalidCommand, "Command is empty"));
                    amqp_destroy_envelope(&envelope);
                    continue;
                }

                std::vector <IMSJob*> jobsList;


                // Convert the message to a job list
                ParseRMQHeaders(envelope, jobsList);

                // clean-up
                amqp_destroy_envelope(&envelope);


                for (auto job : jobsList) {
                    job->DestroyActor = this;
                    m_dispatcher->Submit(job);
                }
                jobsList.clear();
            }

            IMS_INFO(m_console, "[RabitMQ]: Listener thread ending...\n");
            Disconnect();
        }

        bool IMSActorRabbitMQ::Connect()
{
            struct IMSConfig::RMQ& config = m_objectPool.GetConfig()->RMQ;

            IMS_INFO(m_console, "     Running on RabbitMQ version \t%s\n",      amqp_version());
            IMS_INFO(m_console, "     Initializing communication at \t%s:%d\n", config.HostName.c_str(), config.HostPort);
            IMS_INFO(m_console, "     Render queue name set to: \t\t%s\n",      config.RenderQueue.c_str());
            IMS_INFO(m_console, "     Done queue name set to: \t\t%s\n",        config.DoneQueue.c_str());


            // Set member variables;
            amqp_clear_and_set_string(m_render_queue_name, config.RenderQueue.c_str());
            amqp_clear_and_set_string(m_done_queue_name, config.DoneQueue.c_str());

            // Build a new amqp connection state structure
            IMS_INFO(m_console, "     Creating connection... %s\n");
            m_conn_state = amqp_new_connection();
            IMS_INFO(m_console, "\t\tsuccess.\n");

            // Create the socket
            IMS_INFO(m_console, "     Creating new socket... ");
            m_socket = amqp_tcp_socket_new(m_conn_state);
            if (!m_socket)
            {
                IMS_ERROR(m_console, "Error creating TCP socket. Is RabbitMQ Server installed and running?\n");
                return false;
            }
            IMS_INFO(m_console, "\t\tsuccess\n");

            // Open the socket
            IMS_INFO(m_console, "     Opening socket... ");
            int status = amqp_socket_open(m_socket, config.HostName.c_str(), config.HostPort);
            if (status)
            {
                IMS_ERROR(m_console, "Error opening TCP socket. Is RabbitMQ Server installed and running?\n");
                return false;
            }
            IMS_INFO(m_console, "\t\t\tsuccess\n");

            // Login to the server 
            IMS_INFO(m_console, "     Logging in... ");
            if (!return_on_amqp_error(amqp_login(
                                        m_conn_state, 
                                        "/", 
                                        0, 
                                        131072, 
                                        0, 
                                        AMQP_SASL_METHOD_PLAIN, 
                                        config.UserName, 
                                        config.Password), "Logging in"))
            {
                IMS_ERROR(m_console, "Error logging in.\n");
                return false;
            }
            IMS_INFO(m_console, "\t\t\tsuccess\n");


            // Open the channel. name it "m_channel_id" and test it.
            IMS_INFO(m_console, "     Opening channel... ");
            amqp_channel_open(m_conn_state, m_channel_id);
            if (!return_on_amqp_error(amqp_get_rpc_reply(m_conn_state), "Opening channel"))
            {
                IMS_ERROR(m_console, "Error opening channel.\n");
                return false;
            }
            IMS_INFO(m_console, "\t\tsuccess\n");

            // Set the PreFetch count to 1
            // Prefetch count is the number of unpacked requests RabbitMQ may pull of the queue.
            IMS_INFO(m_console, "     Setting prefetch count to %d", config.PrefetchCount);
            amqp_basic_qos(m_conn_state, m_channel_id, 0, config.PrefetchCount, 0);
            if (!return_on_amqp_error(amqp_get_rpc_reply(m_conn_state), "Setting prefetch count"))
            {
                IMS_ERROR(m_console, "Error setting prefetch count.\n");
                return false;
            }
            IMS_INFO(m_console, "\tsuccess\n");

            // Start the consumption. test it
            IMS_INFO(m_console, "     Starting consumption...");
            amqp_basic_consume(m_conn_state, m_channel_id, m_render_queue_name, m_consumer_tag, 0, 0, 0, amqp_empty_table);
            if (!return_on_amqp_error(amqp_get_rpc_reply(m_conn_state), "Consuming"))
            {
                IMS_ERROR(m_console, "Error starting RabbitMQ consumption.\n");
                return false;
            }
            IMS_INFO(m_console, "\t\tsuccess\n");

            IMS_INFO(m_console, "     RabbitMQ listener initialized\n");

            return true;
        }

        void IMSActorRabbitMQ::Disconnect()
        {
            if (0 != m_render_queue_name.len)
            {
                amqp_bytes_free(m_render_queue_name);
                amqp_bytes_zero(&m_render_queue_name);
            }
            if (0 != m_done_queue_name.len)
            {
                amqp_bytes_free(m_done_queue_name);
                amqp_bytes_zero(&m_done_queue_name);
            }

            if (!return_on_amqp_error(amqp_channel_close(m_conn_state, 1, AMQP_REPLY_SUCCESS), "Closing channel"))    {
                return;
            }

            if (!return_on_amqp_error(amqp_connection_close(m_conn_state, AMQP_REPLY_SUCCESS), "Closing connection")) {
                return;
            }

            if (!return_on_error(amqp_destroy_connection(m_conn_state), "Ending connection"))  {
                return;
            }
        }

        void IMSActorRabbitMQ::amqp_clear_and_set_string(amqp_bytes_t& amqpString, const char* str)
        {
            if (0 != amqpString.len)  {
                amqp_bytes_free(amqpString);
                amqp_bytes_zero(&amqpString);
            }
            amqpString = amqp_bytes_malloc_dup(amqp_cstring_bytes(str));
        }

        //---------------------------------------------------------------------------------------------
        //	Extensions to RabbitMQ API
        //---------------------------------------------------------------------------------------------

        //	amqp_bytes_zero: Only zeros the structure. Does NOT clear memory.
        void IMSActorRabbitMQ::amqp_bytes_zero(amqp_bytes_t* bytes)
        {
            if (nullptr == bytes)
            {
                return;
            }
            bytes->bytes = nullptr;
            bytes->len = 0;
        }

        bool IMSActorRabbitMQ::return_on_amqp_error(amqp_rpc_reply_t x, char const *context)
        {
            switch (x.reply_type) {
            case AMQP_RESPONSE_NORMAL:
                return true;

            case AMQP_RESPONSE_NONE:
                IMS_ERROR(m_console, "%s: missing RPC reply type!\n", context);
                break;

            case AMQP_RESPONSE_LIBRARY_EXCEPTION:
                IMS_ERROR(m_console, "%s: %s\n", context, amqp_error_string2(x.library_error));
                break;

            case AMQP_RESPONSE_SERVER_EXCEPTION:
                switch (x.reply.id) {
                case AMQP_CONNECTION_CLOSE_METHOD: {
                                                       amqp_connection_close_t *m = (amqp_connection_close_t *)x.reply.decoded;
                                                       IMS_ERROR(m_console, "%s: server connection error %uh, message: %.*s\n",
                                                           context,
                                                           m->reply_code,
                                                           (int)m->reply_text.len, (char *)m->reply_text.bytes);
                                                       break;
                }
                case AMQP_CHANNEL_CLOSE_METHOD: {
                                                    amqp_channel_close_t *m = (amqp_channel_close_t *)x.reply.decoded;
                                                    IMS_ERROR(m_console, "%s: server channel error %uh, message: %.*s\n",
                                                        context,
                                                        m->reply_code,
                                                        (int)m->reply_text.len, (char *)m->reply_text.bytes);
                                                    break;
                }
                default:
                    IMS_ERROR(m_console, "%s: unknown server error, method id 0x%08X\n", context, x.reply.id);
                    break;
                }
                break;
            }

            return false;
        }


        bool IMSActorRabbitMQ::return_on_error(int x, char const *context)
        {
            if (x < 0) {
                IMS_ERROR(m_console, "%s: %s\n", context, amqp_error_string2(x));
                return false;
            }
            return true;
        }

        idm::ims::IMSActorRabbitMQ::response_t IMSActorRabbitMQ::BuildErrorMessage(uint64_t requestID, EErrorCode errorCode, const std::string& message)
        {

            response_t r;

            r.requestID = requestID;
            r.errorCode = errorCode;
            r.message = message;

            return r;
        }

        bool IMSActorRabbitMQ::ParseRMQHeaders(const amqp_envelope_t& envelope, std::vector<IMSJob*>& jobsList)
        {
            /*********************************************************************
             We will parse the RabbitMQ message here
             We assume that the message is transfered in the following form
                header[0]: <key> : <value>
                 ...
                header[M]: <key> : <value>
                payload


                Currently, we support the below keys only:
                  - cmd
                  - user_data

                Any key beyond the supported keys won't be accepted. 
                Any key should appear only once in the header entries

            **********************************************************************/
            

            bool isCommandFound = false;
            bool isUserDataFound = false;
            std::string command;
            HeaderMap headers;
            std::string payload;
            EErrorCode errorCode = ErrorCode_NoErrors;
            std::string errorMessage;

            // Parse headers
            for (int i = 0; i < envelope.message.properties.headers.num_entries; ++i)
            {
                // Get header entry (key, value)
                amqp_bytes_t_ amqpKey = envelope.message.properties.headers.entries[i].key;
                amqp_field_value_t amqpVal = envelope.message.properties.headers.entries[i].value;

                // make sure it's AMQP_FIELD_KIND_UTF8
                if (amqpVal.kind != AMQP_FIELD_KIND_UTF8)
                {
                    std::string msg = std::string("Header key ") + 
                                      amqp_bytes_to_str(amqpKey) + 
                                      std::string(" has value that is not a utf-8 string.");

                    IMS_ERROR(m_console, "%s\n", msg.c_str());
                    m_resultsQueue.enqueue(BuildErrorMessage(envelope.delivery_tag, ErrorCode_InvalidCommand, msg));

                    return false;
                }

                
                if (amqpVal.value.bytes.len == 0)
                {
                    std::string msg = "Message contains \"cmd\" header with no value.";
                    IMS_ERROR(m_console, "%s\n", msg.c_str());
                    m_resultsQueue.enqueue(BuildErrorMessage(envelope.delivery_tag, ErrorCode_InvalidCommand, msg));

                    return false;
                }


                // At this point we know that our value is from the correct type and has content
                std::string key   = amqp_bytes_to_str(amqpKey);
                std::string value = amqp_utf8_field_to_str(amqpVal);
                std::transform(key.begin(), key.end(), key.begin(), ::tolower);


                IMS_INFO(m_console, "[RabbitMQ]:   Header: key = %s | value = %s\n", key.c_str(), value.c_str());
                if (key == IDM_REQUEST_TAG_CMD) 
                {
                    if (isCommandFound) {
                        std::string msg = "Message contains duplicate \"cmd\" headers.";
                        IMS_ERROR(m_console, "%s\n", msg.c_str());
                        m_resultsQueue.enqueue(BuildErrorMessage(envelope.delivery_tag, ErrorCode_InvalidCommand, msg));

                        return false;
                    }
                    isCommandFound = true;
                    command = value;
                }
                else if (key == IDM_REQUEST_TAG_USER_DATA) {
                    if (isUserDataFound) {
                        std::string msg = "Message contains duplicate \"user_data\" headers.";
                        IMS_ERROR(m_console, "%s\n", msg.c_str());
                        m_resultsQueue.enqueue(BuildErrorMessage(envelope.delivery_tag, ErrorCode_InvalidCommand, msg));

                        return false;
                    }
                    isUserDataFound = true;
                    headers[key] = value;
                }
                else {
                    std::string msg = "Message contains unsupported key " + key + " in message headers.";
                    IMS_ERROR(m_console, "%s\n", msg.c_str());
                    m_resultsQueue.enqueue(BuildErrorMessage(envelope.delivery_tag, ErrorCode_InvalidCommand, msg));

                    return false;
                }
            }

            // grab the body
            payload = amqp_bytes_to_str(envelope.message.body);
            
            IMS_INFO(m_console, "[RabbitMQ]:   Payload: %s\n", payload.c_str());

            if (!IMSCommandParser::CreateIMSJobsList(envelope.delivery_tag, command, payload, headers, jobsList, errorCode, errorMessage))
            {
                IMS_ERROR(m_console, "%s\n", errorMessage.c_str());
                m_resultsQueue.enqueue(BuildErrorMessage(envelope.delivery_tag, errorCode, errorMessage));

                return false;
            }

            return true;
        }


        std::string IMSActorRabbitMQ::amqp_bytes_to_str(const amqp_bytes_t_& key)
        {
            return std::string(static_cast<char*>(key.bytes), key.len);
        }

        std::string IMSActorRabbitMQ::amqp_utf8_field_to_str(amqp_field_value_t val)
        {
            if (val.kind == AMQP_FIELD_KIND_UTF8) {
                return std::string(static_cast<char*>(val.value.bytes.bytes), val.value.bytes.len);
            }
            
            return "";
        }

        //--------------------------------------------------------------------------------------------

    }
}