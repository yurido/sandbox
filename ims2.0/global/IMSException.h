#ifndef IMS_EXCEPTION_H
#define IMS_EXCEPTION_H
#pragma once

#include "exceptions/IDMException.h"

#define IMS_THROW_ERROR( msg ) do { \
    throw idm::IDMException(msg); \
} while (0)


#define IMS_THROW_ERROR_ERRCODE( dest, errorCode, msg ) do { \
    (dest)->Write("Error code: %\n", errorCode); \
    throw idm::IDMException(msg); \
} while (0)


#endif // IMS_EXCEPTION_H