#ifndef IMS_VERSION_H
#define IMS_VERSION_H
#pragma once

#define IMS_VERSION_MAJOR 0
#define IMS_VERSION_MINOR 0
#define IMS_VERSION_PATCH 0
#define IMS_VERSION_BUILD 0

#define STR_HELPER(x) #x
#define IDM_concat_ver(maj,min,patch,gen) STR_HELPER(maj) "." STR_HELPER(min) "." STR_HELPER (patch) " " "G-" STR_HELPER(build)

#define IMS_VERSION IDM_concat_ver(IMS_VERSION_MAJOR,IMS_VERSION_MINOR,IMS_VERSION_PATCH,IMS_VERSION_BUILD)

#endif // IMS_VERSION_H