#ifndef IMS_ERRORS_H
#define IMS_ERRORS_H
#pragma  once

#include <stdint.h>

namespace idm {
    namespace ims {
        typedef enum : uint32_t
        {
            ErrorCode_NoErrors,				    // 0
            ErrorCode_FailedToOpenIDMFile,		// 1
            ErrorCode_FailedToOpenJsonFile,	    // 2
            ErrorCode_IDMParsingError,			// 3
            ErrorCode_JsonParsingError,		    // 4
            ErrorCode_UserSceneIdError,		    // 5
            ErrorCode_UserPlaceHolderIdError,	// 6
            ErrorCode_UserMissingAttribute,	    // 7
            ErrorCode_AssetLoadingError,		// 8
            ErrorCode_FailedToOpenOutFile,		// 9
            ErrorCode_InvalidCommand,			// 10
            ErrorCode_InvalidPath,				// 11
            ErrorCode_NetworkConnectionError,	// 12
            ErrorCode_InternalError,			// 13
            ErrorCode_NVENCError,				// 14
            ErrorCode_CorruptMessage,			// 15
            ErrorCode_UnsupportedVersion,       // 16
            ErrorCode_ImsMuxerError,			// 17
        } EErrorCode;

        // Supported tag ID/Message  pair
        typedef struct tagImsErrorString {
            EErrorCode   ErrorCode;
            const char*  Message;
        } TImsErrortring, *PImsErrorString;

        // Table representation of supported tags (ID, Name)
        static const TImsErrortring IMSErrorMappingTable[] =
        {
            { ErrorCode_NoErrors,				    "No Errors "                    },
            { ErrorCode_FailedToOpenIDMFile,		"Failed To Open IDM File "      },
            { ErrorCode_FailedToOpenJsonFile,	    "Failed To Open Json File "     },
            { ErrorCode_IDMParsingError,			"IDM Parsing Error "            },
            { ErrorCode_JsonParsingError,		    "Json Parsing Error "           },
            { ErrorCode_UserSceneIdError,		    "User SceneId Error "           },
            { ErrorCode_UserPlaceHolderIdError,	    "User PlaceHolder Id Error "    },
            { ErrorCode_UserMissingAttribute,	    "User Missing Attribute "       },
            { ErrorCode_AssetLoadingError,		    "Asset Loading Error "          },
            { ErrorCode_FailedToOpenOutFile,		"Failed To Open Output File "   },
            { ErrorCode_InvalidCommand,			    "Invalid Command "              },
            { ErrorCode_InvalidPath,				"Invalid Path "                 },
            { ErrorCode_NetworkConnectionError,	    "Network Connection Error "     },
            { ErrorCode_InternalError,			    "Internal Error "               },
            { ErrorCode_NVENCError,				    "NVENC Error "                  },
            { ErrorCode_CorruptMessage,			    "Corrupt Message Error "        },
            { ErrorCode_UnsupportedVersion,         "Unsupported file version "     },
            { ErrorCode_ImsMuxerError,			    "Muxer error "                  },

        };
    }
}

#define IMSErrorMappingTable_SIZE (sizeof(IMSErrorMappingTable)/sizeof(TImsErrortring))
#endif	//IMS_ERRORS_H