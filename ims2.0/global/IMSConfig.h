#ifndef IMS_CONFIG_H
#define IMS_CONFIG_H
#pragma once
#include <string>
#include <stdint.h>

namespace idm
{
    namespace ims
    {
        struct IMSConfig 
        {
            struct Generic {
                std::string ConfigFilePath          = "";
                std::string LogPath                 = "logs/ImsLog.txt";
                std::string AutosPath               = "";
                bool AutoShutdown                   = false;
                uint32_t ConsoleVerbosity           = 3;  // EVerbosityLevel
                uint32_t LogVerbosity               = 3;  // EVerbosityLevel
            } Generic;

            struct CLI {
                bool Enable = true;
            } CLI;

            struct ZMQ {
                bool Enable = true;
                std::string JobsSockAddr = "tcp://127.0.0.1:5555";
                bool JobsSockBindConnect = false;
                uint32_t  JobsSockType = 7;
                std::string ResultsSockAddr = "tcp://127.0.0.1:5555";
                bool ResultsSockBindConnect = false;
                uint32_t  ResultsSockType = 8;

            } ZMQ;

            //static member initialization.
            struct RMQ {
                bool Enable                 = true;
                std::string UserName        = "guest";
                std::string Password        = "guest";
                std::string HostName        = "localhost";
                std::string RenderQueue     = "done_queue";
                std::string DoneQueue       = "render_queue";
                uint32_t HostPort           = 5672;
                uint32_t PrefetchCount      = 1;
            } RMQ;

            struct  Encoders {
                bool EncoderNVENC           = true;
                bool EncoderTGA             = false;
                bool EncoderYUV             = false;
                bool DummyEncoder           = false;
                std::string NVENCConfigPath = "nvenconfig.xml";
            } Encoders;


            struct System {
                uint32_t AssetMemoryLimit = 2000;
                struct Actors {
                    struct Commander {
                        uint32_t NumThreads = 1;
                    } Commander;

                    struct Dispatcher {
                        uint32_t NumThreads = 1;
                    } Dispatcher;

                    struct Finalizer {
                        uint32_t NumThreads = 1;
                    } Finalizer;

                    struct Parser {
                        uint32_t NumThreads = 5;
                    } Parser;

                    struct Render {
                        uint32_t NumThreads = 1;
                    } Render;

                    struct Tester {
                        uint32_t NumThreads = 7;
                    } Tester;

                    struct ZeroMQ {
                        uint32_t NumThreads = 1;
                    } ZeroMQ;

                    struct RabbitMQ {
                        uint32_t NumThreads = 1;
                    } RabbitMQ;

                    struct CLI {
                        uint32_t NumThreads = 1;
                    } CLI;

                } Actors;

            } System;

            bool helpScreen = false;

        };
    }
}
#endif // IMS_CONFIG_H