#ifndef IMS_JSON_READER_H
#define IMS_JSON_READER_H

#include "rapidjson/istreamwrapper.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"
#include "utils/IDMFileSysUtils.h"
#include <fstream>
#include "objectpool/IMSObjectPool.h"


namespace idm {
    namespace ims {
        namespace json_reader {

            bool ReadJSONFile(const char* filepath, rapidjson::Document& root)
            {
                IMSObjectPool& objectPool = IMSObjectPool::GetInstance();
                IMSPrint* console = objectPool.GetConsole();

                // try to open the file
                std::ifstream jsonStream;
                if (!idm::FileSysUtils::OpenInputStream(filepath, jsonStream)) 
                {
                    IMS_ERROR_FULL(console, "Error opening file at: '%s'\n", filepath);
                    return false;
                }

                rapidjson::IStreamWrapper isw(jsonStream);
                if (root.ParseStream<rapidjson::kParseCommentsFlag>(isw).HasParseError())
                {
                    IMS_ERROR_FULL(console, "[ERROR]: Failure parsing file %s, Error(offset %u): %s\n", filepath,
                        rapidjson::GetParseError_En(root.GetParseError()));
                    return false;
                }

                return true;
            }

            bool readIntProperty(rapidjson::Value& jsonValue, const char* name, int32_t& property)
            {
                if (jsonValue.HasMember(name))
                {
                    rapidjson::Value& value = jsonValue[name];

                    if (value.IsInt()) {
                        property = value.GetInt();
                        return true;
                    }
                }

                IMS_ERROR_FULL(IMSObjectPool::GetInstance().GetConsole(), "Failed parsing JSON value (%s)\n", name);

                return false;
            }


            bool readIntProperty(rapidjson::Value& jsonValue, const char* name, uint32_t& property)
            {
                if (jsonValue.HasMember(name))
                {
                    rapidjson::Value& value = jsonValue[name];

                    if (value.IsUint()) {
                        property = value.GetUint();
                        return true;
                    }
                }

                IMS_ERROR_FULL(IMSObjectPool::GetInstance().GetConsole(), "Failed parsing JSON value (%s)\n", name);

                return false;
            }

            bool readFloatProperty(rapidjson::Value& jsonValue, const char* name, float& property)
            {
                if (jsonValue.HasMember(name))
                {
                    rapidjson::Value& value = jsonValue[name];

                    if (value.IsFloat()) {
                        property = value.GetFloat();
                        return true;
                    }
                }

                IMS_ERROR_FULL(IMSObjectPool::GetInstance().GetConsole(), "Failed parsing JSON value (%s)\n", name);

                return false;
            }

            bool readBoolProperty(rapidjson::Value& jsonValue, const char* name, bool& property)
            {
                if (jsonValue.HasMember(name))
                {
                    rapidjson::Value& value = jsonValue[name];

                    if (value.IsBool()) {
                        property = value.GetBool();
                        return true;
                    }
                }

                IMS_ERROR_FULL(IMSObjectPool::GetInstance().GetConsole(), "Failed parsing JSON value (%s)\n", name);

                return false;

            }

            bool readStringProperty(rapidjson::Value& jsonValue, const char* name, const char*& property)
            {
                if (jsonValue.HasMember(name))
                {
                    rapidjson::Value& value = jsonValue[name];

                    if (value.IsString()) {
                        property = value.GetString();
                        return true;
                    }
                }

                IMS_ERROR_FULL(IMSObjectPool::GetInstance().GetConsole(), "Failed parsing JSON value (%s)\n", name);

                return false;
            }


            bool readStringProperty(rapidjson::Value& jsonValue, const char* name, std::string& property)
            {
                if (jsonValue.HasMember(name))
                {
                    rapidjson::Value& value = jsonValue[name];

                    if (value.IsString()) {
                        property = value.GetString();
                        return true;
                    }
                }

                IMS_ERROR_FULL(IMSObjectPool::GetInstance().GetConsole(), "Failed parsing JSON value (%s)\n", name);

                return false;
            }



        }
    }
}


#endif // IMS_JSON_READER_H
