#ifndef IMS_TYPES_H
#define IMS_TYPES_H

#include <stdint.h>

typedef enum : uint32_t
{
    RequestType_Render,
    RequestType_ListAssets,
    RequestType_ClearAssets,
    RequestType_JobStatus,
    RequestType_Shutdown,

    // -- Do not add requests beyond this point
    RequestType_Test,
    RequestType_Count

} ERequestType;

typedef enum : uint32_t
{
    // -- Render Job States
    JobRenderState_Create,
    jobRenderState_Init,
    jobRenderState_Parse,
    JobRenderState_Render,
    JobRenderState_Finalize,
    JobRenderState_Destroy,
    JobRenderState_Stop,


    // -- Do not add states beyond this point
    JobRenderState_Test,
    JobRenderState_Count

} EJobRenderState;


typedef enum : uint32_t
{
    JobStatus_Success = 0,
    JobStatus_Failure
} EJobStatus;

typedef enum : uint32_t
{
    ProtocolType_CLI,
    ProtocolType_ZeroMQ,
    ProtocolType_RabitMQ,
} EProtocolType;



typedef enum : uint32_t
{
    // --
    ActorType_ZeroMQ,

    // --
    ActorType_Parser,
    ActorType_Render,
    ActorType_Finalize,
    ActorType_Commander,

    // --
    ActorType_Dispatcher,

    // -- Do not add Actors beyond this point
    ActorType_Test,
    ActorType_Count

} EActorType;



#endif // IMS_TYPES_H



