#ifndef IMS_WORKER_ZEROMQ_H
#define IMS_WORKER_ZEROMQ_H
#pragma once

#include "workers/IMSWorker.h"
#include "print/IMSPrint.h"



namespace idm
{
    namespace ims
    {
        class IMSWorkerZeroMQ : public IMSWorker
        {
        public:
            IMSWorkerZeroMQ(IMSPrint* console) : IMSWorker(console) { m_workerName = "ZeroMQ"; };
            ~IMSWorkerZeroMQ() {};

            virtual void operator()(size_t id, IMSJob * job)
            {
                IMS_TRACE(m_console, "Job (%d) accepted at worker (%s)\n", job->GetJobID(), m_workerName.c_str());

                job->DestroyActor = job->CurrentActor;

                job->SetState(jobRenderState_Init);

                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            };

        private:

        };
    }
}

#endif // IMS_WORKER_ZEROMQ_H