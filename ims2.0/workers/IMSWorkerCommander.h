#ifndef IMS_WORKER_COMMANDER_H
#define IMS_WORKER_COMMANDER_H
#pragma once

#include "workers/IMSWorker.h"
#include "print/IMSPrint.h"



namespace idm
{
    namespace ims
    {
        class IMSWorkerCommander : public IMSWorker
        {
        public:
            IMSWorkerCommander(IMSPrint* console) : IMSWorker(console) { m_workerName = "Commander"; };
            ~IMSWorkerCommander() {};

            virtual void operator()(size_t id, IMSJob * job)
            {
                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            };

        private:

        };
    }
}

#endif // IMS_WORKER_COMMANDER_H