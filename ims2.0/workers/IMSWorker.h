#ifndef IMS_WORKER_H
#define IMS_WORKER_H
#pragma once
#include <condition_variable>
#include "threadsafequeue/concurrentqueue.h"

#include "job/IMSJob.h"
#include "print/IMSPrint.h"



namespace idm
{
    namespace ims
    {
        /*!
         * \class Worker
         *
         * \brief The base class for all the worker (processing) classes. Inherit this class if you
         *        need to provide new processing functionality to be executed by an Actor class
         *
         * \author yurido
         * \date August 2017
         */
        class IMSWorker
        {
        public:
            /*!
             * \brief Virtual destructor to make the file virtual
             */
            virtual ~IMSWorker() {};

            /*!
             * \brief Default constructor. 
             */
            IMSWorker(IMSPrint* console) : m_console(console)  {};

            /*!
            * \brief Initialization function. The Worker should be initialized before it cab be used
            *
            * \param[in] readyQueue - a queue to place the jobs in after process completion
            * \param[in] signal - conditional variable to notify on process completion
            * \return
            */
            void Init(tscq::ConcurrentQueue<IMSJob*>* readyQueue, std::condition_variable* signal)
            {
                m_readyQueue = readyQueue;
                m_cond = signal;
            };
    
            /*!
             * \brief Virtual operator(). This function called by the processing thread to start the job
             *        Must be overridden by the inheriting function. 
             *
             * \param[in] id - the calling thread id
             * \param[in] job - the job to be processed
             * \return
             * \note  The implementation of this function should ALWASY call the Done function
             */
            virtual void operator()(size_t id, IMSJob * job) {

                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            };

        protected:

            /*!
             * \brief Call this function when job processing is completed
             *
             * \param[in] job - a job to complete
             */
            void Done(IMSJob* job) const {
                m_readyQueue->enqueue(job);
                m_cond->notify_all();
            }

            tscq::ConcurrentQueue<IMSJob*>* m_readyQueue;
            std::condition_variable* m_cond;

            std::string m_workerName = "IMSWorker";
            IMSPrint*  m_console;
        };
    }
}
#endif // IMS_WORKER_H
