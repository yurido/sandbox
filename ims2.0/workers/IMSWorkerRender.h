#ifndef IMS_WORKER_RENDER_H
#define IMS_WORKER_RENDER_H
#pragma once

#include "workers/IMSWorker.h"
#include "print/IMSPrint.h"



namespace idm
{
    namespace ims
    {
        class IMSWorkerRender : public IMSWorker
        {
        public:
            IMSWorkerRender(IMSPrint* console) : IMSWorker(console)  { m_workerName = "Render"; };
            ~IMSWorkerRender() {};

            virtual void operator()(size_t id, IMSJob * job)
            {
                IMS_TRACE(m_console, "Job (%d) accepted at worker (%s)\n", job->GetJobID(), m_workerName.c_str());

                job->SetState(JobRenderState_Render);

                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            };

        private:

        };
    }
}

#endif // IMS_WORKER_RENDER_H