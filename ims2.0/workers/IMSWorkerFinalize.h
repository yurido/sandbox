#ifndef IMS_WORKER_FINALIZE_H
#define IMS_WORKER_FINALIZE_H
#pragma once

#include "workers/IMSWorker.h"
#include "print/IMSPrint.h"



namespace idm
{
    namespace ims
    {
        class IMSWorkerFinalize : public IMSWorker
        {
        public:
            IMSWorkerFinalize(IMSPrint* console) : IMSWorker(console)  { m_workerName = "Finalizer"; };
            ~IMSWorkerFinalize() {};

            virtual void operator()(size_t id, IMSJob * job)
            {
                IMS_TRACE(m_console, "Job (%d) accepted at worker (%s)\n", job->GetJobID(), m_workerName.c_str());

                job->SetState(JobRenderState_Finalize);
                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            };

        private:

        };
    }
}

#endif // IMS_WORKER_FINALIZE_H