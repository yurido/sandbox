#ifndef IMS_WORKER_DISPATCHER_H
#define IMS_WORKER_DISPATCHER_H
#pragma once

#include "workers/IMSWorker.h"

#include <string>
#include "job/IMSJob.h"
#include "print/IMSPrint.h"
#include "objectpool/IMSObjectPool.h"



namespace idm
{
    namespace ims
    {
        class IMSWorkerDispatcher : public IMSWorker
        {
        public:
            IMSWorkerDispatcher(IMSPrint* console) : IMSWorker(console)
            {
                m_workerName = "Dispatcher";

                IMSObjectPool& objectPool = IMSObjectPool::GetInstance();

                // Initialize references to all Actors in the system
                m_parserActor    = objectPool.GetActor(ActorType_Parser);
                m_renderActor    = objectPool.GetActor(ActorType_Render);
                m_finalizeActor  = objectPool.GetActor(ActorType_Finalize);
                m_commanderActor = objectPool.GetActor(ActorType_Commander);
                m_zeroMQActor    = objectPool.GetActor(ActorType_ZeroMQ);

                m_console = objectPool.GetConsole(); 

            };
            ~IMSWorkerDispatcher() {};

            virtual void operator()(size_t id, IMSJob * job)
            {
                IMS_TRACE(m_console, "Job (%d) accepted at worker (%s)\n", job->GetJobID(), m_workerName.c_str());

                job->CallerActor = job->CurrentActor;

                // Naive implementation, 
                if (job->GetStatus() != JobStatus_Success) {
                    m_finalizeActor->Submit(job);
                }
                else if (job->GetRequestType() == RequestType_ListAssets) {

                }
                else if (job->GetRequestType() == RequestType_ClearAssets) {

                }
                else if (job->GetRequestType() == RequestType_Shutdown) {

                }
                else if (job->GetRequestType() == RequestType_Render) {
                    switch (job->GetState())
                    {
                    case JobRenderState_Create:
                        {
                            EProtocolType protocol = job->GetProtocolType();
                            if (protocol == ProtocolType_ZeroMQ) {
                                m_zeroMQActor->Submit(job);
                            }
                            else if (protocol == ProtocolType_RabitMQ) {

                            }
                            else if (protocol == ProtocolType_RabitMQ) {

                            }
                        }
                        break;

                    case jobRenderState_Init:
                        m_parserActor->Submit(job);
                        break;

                    case jobRenderState_Parse:
                        m_renderActor->Submit(job);
                        break;

                    case JobRenderState_Render:
                        m_finalizeActor->Submit(job);
                        break;

                    case JobRenderState_Stop:
                        m_finalizeActor->Submit(job);
                        break;

                    case JobRenderState_Finalize:
                       // job->DestroyActor->Submit(job);
                        break;


                    default:
                        IMS_ERROR(m_console, "Unsupported render state (%d)", job->GetRequestType());
                        break;
                    }
                }
                else {
                    IMS_ERROR(m_console, "Unsupported request type (%d)", job->GetRequestType());
                }


                // -- Dispatcher, is the only worker that doesn't call to Done
                // -- function, as there is no ReadyQueue in the dispatcher.
               // Done(job);
            };

        private:

            IIMSActor* m_parserActor;
            IIMSActor* m_renderActor;
            IIMSActor* m_finalizeActor;
            IIMSActor* m_commanderActor;
            IIMSActor* m_zeroMQActor;

            

            
        };
    }
}

#endif // IMS_WORKER_DISPATCHER_H
