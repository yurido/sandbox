#ifndef IMS_WORKER_TEST_H
#define IMS_WORKER_TEST_H
#pragma once
#include "IMSWorker.h"

#include <string>
#include "job/IMSJob.h"
#include "print/IMSPrint.h"


namespace idm
{
    namespace ims
    {
        typedef enum : uint32_t 
        {
            IMSTest_ActorFlow,

            // -- Do no add tests beyond this point
            IMSTest_Count

        } EIMSTest;

        class IMSWorkerTest : public IMSWorker
        {
        public:
            IMSWorkerTest(IMSPrint* console) : IMSWorker (console){ m_workerName = "Tester"; };
            ~IMSWorkerTest() {};

            virtual void operator()(size_t id, IMSJob * job)  
            {
                IMS_TRACE(m_console, "Job (%d) accepted at worker (%s)\n", job->GetJobID(), m_workerName.c_str());

                // We want to test that each job returned from the actor with
                // new state (JobRenderState_Test) - meaning, the job is processed
                job->SetState(JobRenderState_Test);


                // -- SHOULD BE CALLED AT THE END OF ANY OVERLOADED OPERATOR() FUNCTION
                Done(job);
            };

        };
    }
}

#endif // IMS_WORKER_TEST_H
