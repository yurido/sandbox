#ifndef IMS_JOB_H
#define IMS_JOB_H
#pragma once
#include <map>
#include <stdint.h>

#include "global/IMSTypes.h"
#include "print/IMSPrint.h"


namespace idm
{
    namespace ims
    {
        class IIMSActor;

        typedef std::map<std::string, std::string> HeaderMap;



        struct IMSJobReport
        {
            uint64_t	m_id                = 0;
            double		m_renderTimeMs      = 0;
            int32_t 	m_framesRendered    = 0;
            uint32_t	m_errorCode         = 0;
            int32_t		m_errorSceneId      = 0;
            std::string	m_message;
        };


        struct IMSJob
        {
            uint64_t GetJobID()                                { return m_jobID; }
                                                               
            void SetRenderState(EJobRenderState state)         { m_lastState = m_state; m_state = state; }
            EJobRenderState GetRenderState() const             { return m_state; }
            EJobRenderState GetLastRenderState() const         { return m_lastState; }
                                                            
            void SetRequestType(ERequestType request)          { m_request = request; }
            ERequestType GetRequestType() const                { return m_request; }
                                                               
            void SetStatus(EJobStatus status)                  { m_status = status; }
            EJobStatus GetStatus() const                       { return m_status; }

            void SetProtocolType(EProtocolType protocol)       { m_protocol = protocol; }
            EProtocolType GetProtocolType() const              { return m_protocol; }


            IMSPrint*      Console = nullptr;
            IMSPrint*      Log     = nullptr;


            // Reference to required Actor objects
            
            IIMSActor* DestroyActor;
            IIMSActor* CallerActor;
            IIMSActor* CurrentActor;

            // Render specific information
            std::string InputFilePath;
            HeaderMap RequestHeaders;

            // Report and status
            IMSJobReport    Report;


            // Implicitly initialize Job and increment job counter
            IMSJob();

        private:

            ERequestType    m_request;
            EJobRenderState m_state;
            EJobRenderState m_lastState;
            EJobStatus      m_status;
            EProtocolType   m_protocol;

            uint64_t m_jobID;
            static uint64_t m_globalJobID;
        };
    }
}
#endif // IMS_JOB_H

