#include "IMSJob.h"

namespace idm
{
    namespace ims
    {

        IMSJob::IMSJob()
            : m_request(RequestType_Count)
            , m_state (JobRenderState_Create)
            , m_lastState(JobRenderState_Create)
            , m_status(JobStatus_Success)
            , m_jobID(++m_globalJobID)
            , m_protocol(ProtocolType_CLI)
            , CallerActor(nullptr)
            , CurrentActor(nullptr)
            , DestroyActor(nullptr)
        {

        }

        // Initialize static Job ID variable
        uint64_t IMSJob::m_globalJobID = 0;
    }
}