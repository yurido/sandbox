#ifndef IMS_COMMAND_PARSER_H
#define IMS_COMMAND_PARSER_H
#pragma once
#include <map>
#include <vector>
#include "global/IMSErrors.h"

#include "job/IMSJob.h"

namespace idm {
    namespace ims {

        #define IDM_REQUEST_TAG_REQID				"request_id"
        #define IDM_REQUEST_TAG_CMD					"cmd"
        #define IDM_REQUEST_TAG_ARG					"arg"
        #define IDM_REQUEST_TAG_ERROR_CODE			"error_code"
        #define IDM_REQUEST_TAG_MESSAGE				"message"
        #define IDM_REQUEST_TAG_USER_DATA			"user_data"	

        #define IDM_RENDERREQUEST_TAG_RENDER_TIME	"render_time"
        #define IDM_RENDERREQUEST_TAG_FRAME_COUNT	"frame_count"
        #define IDM_RENDERREQUEST_TAG_FPS			"fps"
        #define IDM_RENDERREQUEST_TAG_ERROR_SCENEID	"error_scene_id"


        class IMSCommandParser {
        public: 
            /*!
            * \brief Parses the input command and creates a vector of IMSJobs
            *
            * \param[in] requestID - unique ID of the request
            * \param[in] command - request to execute
            * \param[in] args - additional arguments
            * \param[in] headers - additional command headers
            * \param[out] jobsList - on success, a list of IMSJobs to process
            * \param[out] errorCode - on failure, unique IMS error code that describes the failure
            * \param[out] errorMessage - on failure, error message that describes the failure
            * \return true on success, false otherwise
            */
            static bool CreateIMSJobsList( uint64_t requestID, 
                                    const std::string& command, 
                                    const std::string& payload, 
                                    HeaderMap headers, 
                                    std::vector<IMSJob*>& jobsList, 
                                    EErrorCode& errorCode, 
                                    std::string& errorMessage);

        private:
            typedef std::map<std::string, ERequestType> RequestMap;

            static RequestMap InitRequestTypeMapping();
            static RequestMap m_requestTypes;

            static ERequestType GetRequestType(const std::string& command);


            static void TrimExtraCharacters(std::string& str);
        };
    }
}

#endif // IMS_COMMAND_PARSER_H