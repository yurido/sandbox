#include "IMSCommandParser.h"
#include <algorithm>
#include "objectpool/IMSObjectPool.h"

namespace idm {
    namespace ims {

        IMSCommandParser::RequestMap IMSCommandParser::m_requestTypes = InitRequestTypeMapping();

        bool IMSCommandParser::CreateIMSJobsList(uint64_t requestID, const std::string& command, const std::string& payload, HeaderMap headers, std::vector<IMSJob*>& jobsList, EErrorCode& errorCode, std::string& errorMessage)
        {
            IMSObjectPool& objectPool = IMSObjectPool::GetInstance();

            // We do not expect any error 
            errorCode = ErrorCode_NoErrors;
            errorMessage = "";

            // Parse the request type and check if it supported
            ERequestType requestType = GetRequestType(command);
            if (requestType == RequestType_Count) 
            {
                IMSPrint* console = objectPool.GetConsole();
                IMS_ERROR(console, "Invalid command: command (%s) not supported.\n", command);

                errorCode = ErrorCode_InvalidCommand;
                errorMessage = std::string("Invalid command: command (") + command + std::string(") not supported.");
                return false;
            }


            // Replace all \r\n EOL sequences to a single character '\r'
            std::string cmdPayload = payload;
            while (cmdPayload.find("\r\n") != std::string::npos) {
                cmdPayload.replace(cmdPayload.find("\r\n"), 2, "\r");
            }

            // Parse the arguments into lines
            std::string line;
            std::vector<std::string> tokensList;
            std::stringstream stream(cmdPayload);

            while (getline(stream, line, '\r'))
            {
                // Remove extra spacing characters
                TrimExtraCharacters(line);

                if (line.empty()) 
                    { continue; }

                tokensList.push_back(line);
            }


            IMSJob* job = objectPool.CreateJob();

            // Setup job relevant data
            job->SetRequestType(requestType);
            job->RequestHeaders = headers;

            // For the render request we expect the user to provide a path
            // to the input file (*.idm or *.json)
            if (requestType == RequestType_Render && tokensList.size() > 0) {
                job->InputFilePath = tokensList[0];
                job->SetRenderState(jobRenderState_Init);
            }

            jobsList.push_back(job);
            

            return true;
        }



        IMSCommandParser::RequestMap IMSCommandParser::InitRequestTypeMapping()
        {
            IMSCommandParser::RequestMap requestTypes;

            requestTypes["unknown"] = RequestType_Count;
            requestTypes["render"] = RequestType_Render;
            requestTypes["assets"] = RequestType_ListAssets;
            requestTypes["clear"] = RequestType_ClearAssets;
            requestTypes["shutdown"] = RequestType_Shutdown;
            requestTypes["quit"] = RequestType_Shutdown;
            requestTypes["exit"] = RequestType_Shutdown;

            return requestTypes;
        }

        ERequestType IMSCommandParser::GetRequestType(const std::string& command)
        {
            // Convert request to lower case (protect against user typo)
            std::string lowerCaseCommand = command;
            std::transform(command.begin(), command.end(), lowerCaseCommand.begin(), ::tolower);

            // Look for the request type ID
            RequestMap::iterator it = m_requestTypes.find(lowerCaseCommand);

            return (it == m_requestTypes.end())
                ? RequestType_Count
                : it->second;
        }

        void IMSCommandParser::TrimExtraCharacters(std::string& str)
        {
            const char* trimChars = " \t\n\v\f\r";

            // Find first occurrence of non special character
            int first = (int)str.find_first_not_of(trimChars);
            if (std::string::npos == first)
            {
                // Regular characters not found.
                str = "";
                return;
            }


            int last = (int)str.find_last_not_of(trimChars);
            if ((0 != first) || (str.size() != (last + 1)))
            {
                str = str.substr(first, (last - first) + 1);
            }
        }


    }
}