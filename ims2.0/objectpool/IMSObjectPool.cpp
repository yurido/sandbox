#include "IMSObjectPool.h"

#include "actors/IMSActorTest.h"
#include "actors/IMSActorZeroMQ.h"
#include "actors/IMSActorParser.h"
#include "actors/IMSActorRender.h"
#include "actors/IMSActorFinalizer.h"
#include "actors/IMSActorCommander.h"
#include "actors/IMSActorDispatcher.h"
#include "global/IMSException.h"

namespace idm
{
    namespace ims
    {

        struct TActorDetails {
            IIMSActor*  actor;
            uint32_t    counter;
        };

        IMSObjectPool::IMSObjectPool()
        {
            m_console.SetVerbosityLevel(VerbosityLevel_Trace);
        }

        IMSObjectPool::~IMSObjectPool()
        {
            Dispose();
        }

        IMSObjectPool& IMSObjectPool::GetInstance()
        {
            static IMSObjectPool instance;

            return instance;
        }

        IIMSActor* IMSObjectPool::GetActor(EActorType type)
        {
            if (type >= ActorType_Count)
            {
                IMS_ERROR(&m_console, "Illegal actor type (%d)", type);
                return nullptr;
            }

            IIMSActor* actor = m_actorsMap[type].actor;

            return actor;
        }

        idm::ims::IMSPrint* IMSObjectPool::GetConsole()
        {
            return &m_console;
        }

        idm::ims::IMSPrint* IMSObjectPool::GetLogger()
        {
            return &m_logger;
        }

        idm::ims::IMSConfig* IMSObjectPool::GetConfig()
        {
            return m_config;
        }

        idm::ims::IMSJob* IMSObjectPool::CreateJob()
        {
            std::lock_guard<std::mutex> lk(m_mutex);

            IMSJob* job = new IMSJob();

            if (job) {
                m_jobsMap[job->GetJobID()] = job;
                job->Console = &m_console;

                // TODO: do we want a single logger for the platform or we want some kind of a unique job logger
                // job->Log = &m_logger;
            }

            return job;
        }

        void IMSObjectPool::DeleteJob(IMSJob* job)
        {
            std::lock_guard<std::mutex> lk(m_mutex);

            TJobsMap::iterator iter = m_jobsMap.find(job->GetJobID());

            if (iter == m_jobsMap.end()) {
                IMS_WARNING(&m_console, "Job (%d) not found in the ObjectPool", job->GetJobID());
                return;
            }

            m_jobsMap.erase(iter);
            delete job;
        }

        void IMSObjectPool::Dispose()
        {
            for (TActorsMap::iterator iter = m_actorsMap.begin(); iter != m_actorsMap.end(); ++iter)
            {
                TActorDetails& details = iter->second;

                if (details.actor)  {
                    details.actor->Stop();
                    delete details.actor;
                }
            }
            m_actorsMap.clear();

            for (auto job : m_jobsMap) {
                DeleteJob(job.second);
            }
            m_jobsMap.clear();

        }

        idm::ims::IIMSActor* IMSObjectPool::CreateActor(EActorType type, uint32_t numThreads)
        {
            IIMSActor* actor = nullptr;
            switch (type) {
            case ActorType_ZeroMQ:
                actor = new IMSActorZeroMQ();
                break;

            case ActorType_Parser:
                actor = new IMSActorParser();
                break;

            case ActorType_Render:
                actor = new IMSActorRender();
                break;

            case ActorType_Finalize:
                actor = new IMSActorFinalizer();
                break;

            case ActorType_Commander:
                actor = new IMSActorCommander();
                break;

            case ActorType_Dispatcher:
                actor = new IMSActorDispatcher();
                break;

            case ActorType_Test:
                actor = new IMSActorTest();
                break;


            case ActorType_Count:
            default:
                IMS_ERROR_FULL(&m_console, "Illegal actor type (%d)", type);
                //IMS_THROW_ERROR;
            }

            actor->Init(numThreads);

            // No suitable worker found
            return actor;
        }

        uint32_t IMSObjectPool::GetNumActorThreads(EActorType type)
        {
            uint32_t numThreads = 1;

            if (type == ActorType_Parser) {
                numThreads = m_config->System.Actors.Parser.NumThreads;
            }
            else if (type == ActorType_Test) {
                numThreads = m_config->System.Actors.Tester.NumThreads;
            }

            return numThreads;
        }

        void IMSObjectPool::Init(IMSConfig* config)
        {
            if (config == nullptr) {
                IMS_ERROR_FULL(&m_console, "Wrong configuration structure\n");
            }

            m_config = config;

            for (uint32_t type = 0; type < ActorType_Count; ++type)
            {
                TActorDetails details;

                details.actor = CreateActor((EActorType)type, GetNumActorThreads((EActorType)type));
                details.counter = 1;

                m_actorsMap[(EActorType)type] = details;
            }


            // Only after all Actors are created we can Start them
            // to ensure that all cross references are valid
            for (auto actor : m_actorsMap)  {
                if (!actor.second.actor->Start())
                {
                    Dispose();
                    IMS_THROW_ERROR("Failed to initialize object pool. Actor Start failure\n");
                }
            }



            m_logger.SetOutputFileStream(config->Generic.LogPath);
            m_logger.SetVerbosityLevel((EVerbosityLevel)config->Generic.LogVerbosity);

            m_console.SetVerbosityLevel((EVerbosityLevel)config->Generic.ConsoleVerbosity);
        }

    }
}
