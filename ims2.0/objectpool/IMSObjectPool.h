#ifndef IMS_OBJECT_POOL_H
#define IMS_OBJECT_POOL_H
#pragma once

#include <map>

#include "global/IMSTypes.h"
#include "actors/IIMSActor.h"
#include "print/IMSPrint.h"
#include "global/IMSConfig.h"



namespace idm
{
    namespace ims
    {

        /*!
         * \class IMSObjectPool
         *
         * \brief The class is responsible for creating and destroying any kind of IMS objects
         *        It designed as a singleton. 
         *
         * \author yurido
         * \date August 2017
         */
        class IMSObjectPool
        {
        public:
            /*!
             * \brief Default destructor
             */
            ~IMSObjectPool();

            /*!
             * \brief Singleton access point
             *
             * \return Reference to an ObjectPool object
             */
            static IMSObjectPool& GetInstance();

            /*!
             * \brief Initialize the object pool with global configuration
             *
             * \param[in] config - pointer to configuration strcuture
             */
            void Init(IMSConfig* config);

            /*!
            * \brief Get pointer to an Actor
            *
            * \param[in] type - actor type to create
            * \return Pointer to Actor's interface on success and null on failure
            */
            IIMSActor* GetActor(EActorType type);

            /*!
             * \brief Creates and returns the pointer to default console output
             * \return Pointer to console object
             */
            IMSPrint*  GetConsole();

            /*!
             * \brief Creates and returns the pointer to the logger. 
             * \return Pointer to logger object
             */
            IMSPrint* GetLogger();


            /*!
             * \brief return a pointer to the configuration object
             */
            IMSConfig* GetConfig();

            /*!
             * \brief Create new IMSJob object
             * \return IMSJob pointer on success. Null pointer otherwise
             */
            IMSJob* CreateJob();

            /*!
             * \brief Destroys IMSJob object
             *
             * \param[in] Pointer to IMSJob to destroy
             */
            void DeleteJob(IMSJob* job);


            /*!
             * \brief Dispose all internal data structures of the Object Pool
             */
            void Dispose();


        private:
            IMSObjectPool();
            IIMSActor* CreateActor(EActorType type, uint32_t numThreads);
            uint32_t GetNumActorThreads(EActorType type);

            std::mutex m_mutex;

            typedef struct TActorDetails TActorDetails;
            typedef std::map<EActorType, TActorDetails>  TActorsMap;
            typedef std::map<uint64_t, IMSJob*> TJobsMap;

            TActorsMap m_actorsMap;
            TJobsMap   m_jobsMap;
            IMSPrint   m_console;
            IMSPrint   m_logger;
            IMSConfig* m_config = nullptr;
        public:
            
        };
    }
}
#endif // IMS_OBJECT_POOL_H

