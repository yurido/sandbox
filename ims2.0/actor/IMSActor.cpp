#include "IMSActor.h"
#include <mutex>


namespace idm
{
    namespace ims
    {

        IMSActor::IMSActor() {}

        IMSActor::IMSActor(IMSWorker* worker, uint32_t numThreads) : m_worker(worker)
        {
            m_numThreads = numThreads;
            Init(worker, numThreads);
        }

        IMSActor::~IMSActor() {}


        void IMSActor::Reset()
        {
            Stop();
            Start();
        }


        void IMSActor::Init(IMSWorker* worker, uint32_t numThreads)
        {
            m_worker = worker;

            // Each worker needs a pointer to the ready queue (for enqueue the processed job)
            // and the conditional variable to signal on completion
            m_worker->Init(&m_readyQueue, &m_readyQueueChange);
        }

        bool IMSActor::Start()
        {
            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // No need to start already started thread, otherwise the old threads will be unhandled
            if (m_isStartted) {
                return true;
            }

            m_isStartted = true;
            m_isStopped = false;

            // Prepare the memory pool
            m_workersPool.restart(m_numThreads);

            // Create Actor's threads
            m_waitingQueueProcessThread = std::thread(&IMSActor::JobStartProcess, this);
            m_readyQueueProcessThread = std::thread(&IMSActor::JobEndProcess, this);

            // Start processing (in case job where submitted before the actor started)
            m_waitingQueueChange.notify_all();
           
            return true;
        }

        bool IMSActor::Stop()
        {
            // We want that only one flow control function be active at a time
            std::lock_guard<std::mutex> lk(m_flowControlMutex);

            // If actor was already stopped, return
            if (m_isStopped) {
                return true;
            }

            m_isStopped = true;
            m_isStartted = false;

    

            // Remove all Jobs from the queue
            // Note! the job that are currently in Actor's queues 
            //       will return to the caller, however, the jobs currently
            //       processed will be dropped. 
            //
            //       The premise: we don't care about the Jobs on stop
            ///      as we shutting down. 

            // The processed job will finish, but waiting threads will be dropped
            m_workersPool.stop(false);

            IMSJob* job = nullptr;
            while (m_waitingQueue.try_dequeue(job))  {job->SetState(JobRenderState_Stop); }
            while (m_readyQueue.try_dequeue(job))    {job->SetState(JobRenderState_Stop); }

            // Notify all Actors thread
            m_waitingQueueChange.notify_all();
            m_readyQueueChange.notify_all();

            // Wait for the Actors thread to complete
            if (m_waitingQueueProcessThread.joinable()) {
                m_waitingQueueProcessThread.join();
            }

            if (m_readyQueueProcessThread.joinable()) {
                m_readyQueueProcessThread.join();
            }

            return true;
        }

        bool IMSActor::Submit(IMSJob* job)
        {
            if (!job || m_isStopped) {
                return false;
            }

            // Add job to waiting queue
            m_waitingQueue.enqueue(job);
            m_waitingQueueChange.notify_all();

            // Store pointer to the Actor
            job->CurrentActor = this;

            return true;
        }


        void IMSActor::JobStartProcess()
        {
            //m_workersPool.restart();

            std::unique_lock<std::mutex> lk(m_waitingQueueMutex);

            while (!m_isStopped)
            {
                // Wait for the change in the waiting queue or for the stop flag
                m_waitingQueueChange.wait(lk, [this]() {return ((m_waitingQueue.size_approx() > 0) || m_isStopped); });

                if (m_isStopped) { return; }
                
                IMSJob* job = nullptr;

                // Schedule all jobs in the waiting queue for processing
                while (m_waitingQueue.size_approx() > 0) 
                {
                    if (!m_waitingQueue.try_dequeue(job)) {
                        // no more jobs in the queue or sporadic wakeup
                        continue;
                    }

                    m_workersPool.push(std::ref(*m_worker), job);
                }

            }
        }


        void IMSActor::JobEndProcess()
        {
            std::unique_lock<std::mutex> lk(m_readyQueueMutex);

            while (!m_isStopped)
            {
                // Wait for the change in the waiting queue or for the stop flag
                m_readyQueueChange.wait(lk, [this]() {return ((m_readyQueue.size_approx() > 0) || m_isStopped); });

                IMSJob* job = nullptr;

                if (m_isStopped) { 
                    // Dequeue all jobs from the ready queue
                    // (in case where added by processing workers after
                    //  the cleanup in Stop function)
                    while (m_readyQueue.try_dequeue(job)) {};
                    return; 
                }

                // Complete all ready jobs
                while (m_readyQueue.size_approx() > 0)
                {
                    if (!m_readyQueue.try_dequeue(job)) {
                        // no more jobs in the queue or sporadic wakeup
                        continue;
                    }

                    // Return the job to the caller
                    if (job->CallerActor) {
                        job->CallerActor->Submit(job);
                    }
                }
            }
        }


    }
}