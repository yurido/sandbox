#pragma once
#ifndef IMS_ACTOR_H
#define IMS_ACTOR_H

#include <thread>
#include <condition_variable>

#include "IIMSActor.h"
#include "threadsafequeue/concurrentqueue.h"
#include "job/IMSJob.h"
#include "threadpool/ctpl_stl.h"

namespace idm
{
    namespace ims
    {
        /*!
         * \class IMSActor
         *
         * \brief The IMSActor is the common implementation of an Actor state. 
         *        The Actor receives a Worker class and number of threads it can open,
         *        meaning that the worker can process thus many jobs simultaneously. 
         *        
         *        From implementation perspective an Actor will hold a threads pool of 
         *        the requested size and for each coming job will execute a worker in a 
         *        separate thread with a Job as a parameter. 
         *
         * \author yurido
         * \date August 2017
         */
        class IMSActor : public IIMSActor
        {
        public:
            /*!
            * \brief Default constructor
            *
            */
            IMSActor();


            /*!
             * \brief Default constructor
             *
             * \param[in] worker - the class that is responsible for the processing of the Jobs inside the Actor
             * \param[in] numThreads - number of threads the worker can run simultaneously 
             * \return
             */
            IMSActor(uint32_t numThreads);


            /*!
             * \brief Default destructor
             */
            virtual ~IMSActor();


            /*!
            * \brief Resets internal state of the actor, clear queues and etc.
            */
            virtual void Reset() override;


            /*!
             * \brief Initialization function
             *
             * \param[in] worker - the class that is responsible for the processing of the Jobs inside the Actor
             * \param[in] numThreads - number of threads the worker can run simultaneously
             * \return
             */
            virtual void Init(uint32_t numThreads) override;


            /*!
             * \brief Start Actor execution (all internal threads are executed)
             * \return true on success false otherwise
             */
            virtual bool Start() override;


            /*!
             * \brief Stops Actor execution. The Actor will wait for all processed Jobs to finish
             *        and only then will stop the threads
             * \return true on success false otherwise
             */
            virtual bool Stop() override;


            /*!
             * \brief Submit new Job to the Actor. The function adds the Job to the waiting queue
             *        and immediately return (non-blocking)
             *
             * \param[in] job - new job to process
             * \return true on success false otherwise
             */
            virtual bool Submit(IMSJob* job) override;
           

            /*!
            * \brief Process the job
            *
            * \param[in] id - thread id. Internal use of the thread pool
            * \param[in] job - job to work on
            */
            virtual void ProcessJob(int id, IMSJob* job) override;


        protected:

            /*!
             * \brief Enqueues the Job to ready queue and signals the correct condition variable
             *
             * \param[in] job - job to process
             */
            void Done(IMSJob* job);

            /*!
             * \brief Ready queue processing function. Take the job form the ready queue and submits 
             *        it back to the caller
             *
             * \return
             */
            void JobEndProcess();


            // Thread safe queues
            tscq::ConcurrentQueue<IMSJob*> m_readyQueue;

            // Condition variables waiting for the queue state change
            std::condition_variable m_readyQueueChange;

            // Mutexes for the condition variables
            std::mutex m_readyQueueMutex;
            std::mutex m_flowControlMutex;

            // Processing threads
            std::thread m_readyQueueProcessThread;

            // Global "Stop" flag (another thread exit condition)
            std::atomic<bool> m_isStopped = false;
            std::atomic<bool> m_isStartted = false;

            // Workers thread pool
            ctpl::thread_pool m_workerThreadPool;
            uint32_t          m_numThreads;

            std::string     m_actorName;
            IMSPrint*       m_console;

        };
    }
}

#endif // IMS_ACTOR_H

