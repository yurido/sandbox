#include "IMSTest_Actor.h"
#include "objectpool/IMSObjectPool.h"
#include "print/IMSPrint.h"

namespace idm
{
    namespace ims
    {
        std::mutex IMSTest_Actor::m_mutex;

        IMSTest_Actor::IMSTest_Actor(size_t numOfJobs) 
            : m_numOfJobs(numOfJobs)
        {
            IMSObjectPool& objectPool = IMSObjectPool::GetInstance();
            m_actor = objectPool.GetActor(ActorType_Test);
            m_console = objectPool.GetConsole();
            m_actorName = "Actor - Test";

            for (size_t i = 0; i < numOfJobs; i++)
            {
                ims::IMSJob* job = objectPool.CreateJob();
                job->CallerActor = dynamic_cast<IIMSActor*>(this);
                m_jobs.push_back(job);
            }
        }


        IMSTest_Actor::~IMSTest_Actor()
        {
            IMSObjectPool& objectPool = IMSObjectPool::GetInstance();

            m_actor->Reset();

            for (auto job : m_jobs) {
                objectPool.DeleteJob(job);
            }
            m_jobs.clear();
        }

        bool IMSTest_Actor::Submit(IMSJob* job)
        {
            std::lock_guard<std::mutex> lk(m_mutex);

            m_jobsCompleted.push_back(job);

            return true;
        }

        bool IMSTest_Actor::Execute()
        {
            // -------------------------------------------------------
            // -- TEST REGULAR FLOW
            // -------------------------------------------------------
            for (auto job : m_jobs) {
                m_actor->Submit(job);
            }

            // Allocate 20 milliseconds for each Job to finish
            std::this_thread::sleep_for(std::chrono::milliseconds(30 * m_numOfJobs));

            // Sort the completed jobs array in the ascending order
            std::sort(m_jobsCompleted.begin(), m_jobsCompleted.end(),
                [](IMSJob* const &l, IMSJob* const &r) { return l->GetJobID() < r->GetJobID(); });


            // We expect that all the jobs returned from the Actor
            if (m_jobsCompleted.size() != m_numOfJobs) {
                return false;
            }

            for (size_t i = 0; i < m_numOfJobs; ++i) 
            {
                // We expect the worker to change the Job status to test
                if (m_jobsCompleted[i]->GetRenderState() != JobRenderState_Test) {
                    return false;
                }

                // We expect the same Job IDs on the returning jobs
                if (m_jobsCompleted[i]->GetJobID() != m_jobs[i]->GetJobID()) {
                    return false;
                }
            }

            // -------------------------------------------------------
            // -- TEST ABORT FLOW
            // -------------------------------------------------------
            m_jobsCompleted.clear();

            for (auto job : m_jobs) {
                m_actor->Submit(job);
            }

            // Allocate 20 milliseconds for each Job to finish
            std::this_thread::sleep_for(std::chrono::milliseconds(20 * m_numOfJobs));

            // We expect that all the jobs returned from the Actor
            if (m_jobsCompleted.size() != m_numOfJobs) {
                return false;
            }

            // All tests passed
            return true;
        }

        void IMSTest_Actor::ProcessJob(int id, IMSJob* job)
        {
        }

    }
}
