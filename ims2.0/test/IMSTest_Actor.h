#ifndef IMS_TEST_H
#define IMS_TEST_H
#pragma once
#include <vector>
#include "job/IMSJob.h"
#include "actors/IMSActor.h"

namespace idm
{
    namespace ims
    {
        class IMSTest_Actor : public IMSActor
        {
        public:
            IMSTest_Actor(size_t numOfJobs);
            virtual ~IMSTest_Actor();

            virtual bool Submit(IMSJob* job) override;

            bool Execute();




            // We need those to implement IIMSInterface only
            virtual void Init(uint32_t numThreads) override  { return; };


            virtual bool Start() override { return true; };


            virtual bool Stop() override { return true; };


            virtual void Reset() override { return; };


            virtual void ProcessJob(int id, IMSJob* job) override;

        private:
            IMSPrint*  m_console;

            size_t    m_numOfJobs;

            static std::mutex  m_mutex;


            IIMSActor* m_actor;


            typedef std::vector<ims::IMSJob*> JobsList;
            JobsList m_jobs;
            JobsList m_jobsCompleted;

        };
    }
}
#endif // IMSTest_h__

