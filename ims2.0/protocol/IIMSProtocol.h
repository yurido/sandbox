#ifndef IMS_PROTOCOL_H
#define IMS_PROTOCOL_H
#pragma once
#include <vector>
#include "job/IMSJob.h"
#include "actors/IMSActor.h"

namespace idm
{
    namespace ims
    {
        class IIMSProtocol : public IIMSActor
        {
        public:
            IIMSProtocol() {};
            virtual ~IIMSProtocol() {};

            /*!
             * \brief Used for submitted completed job to the protocol class
             *        The protocol implementation reports the results to the 
             *        requester and destroys the Job (as implemented is the
             *        actual creator of the Job itself)
             *
             * \param[in] job - IMS job with final result and status
             */
            virtual bool Submit(IMSJob* job) = 0;



            // ------------------------------------------------------------------------------------------
            // We need those to implement IIMSActor Interface only
            virtual void Init(uint32_t numThreads) override  { return; };


            virtual bool Start() override { return true; };


            virtual bool Stop() override { return true; };


            virtual void Reset() override { return; };
            // ------------------------------------------------------------------------------------------
        };
    }
}
#endif // IMS_PROTOCOL_H

