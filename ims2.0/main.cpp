#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <string>


#include "test/IMSTest_Actor.h"

#include "objectpool/IMSObjectPool.h"
#include "IDMEngineVersion.h"
#include "global/IMSVersion.h"
#include "commandline/IMSCLReader.h"
#include "print/IMSPrint.h"


using namespace idm;
using namespace ims;


//void TestIMSPrint() {
//    ims::IMSPrint print;
//    ims::IMSPrint print2;
//
//    std::string f1 = "C:/yurido_devenv/repos/idomoo_dev/ims2.0/x64/Debug/file1.txt";
//    std::string f2 = "C:/yurido_devenv/repos/idomoo_dev/ims2.0/x64/Debug/file2.txt";
//    std::string f3 = "C:/yurido_devenv/repos/idomoo_dev/ims2.0/x64/Debug/file1.txt";
//    
//   // print.SetOutputFileStream(f2);
//   // print.Test("test f2\n");
//    print.SetOutputFileStream(f1);
//    print2.SetOutputFileStream(f3);
//    print.SetOutputFileStream(f2);
//}

void RunTests()
{
    IMSObjectPool& objectPool = IMSObjectPool::GetInstance();
    IMSPrint* console = objectPool.GetConsole();
    console->SetVerbosityLevel(VerbosityLevel_Info);

    {
        IMSTest_Actor testActor(12);
        IMS_INFO(console, "Simple Test: %s\n", (testActor.Execute()) ? "PASSED" : "FAILED");
    }

    {
        IMSTest_Actor testActor(20);
        IMS_INFO(console, "Continuation test: %s\n", (testActor.Execute()) ? "PASSED" : "FAILED");
    }


    {
        IMSTest_Actor testActor(20);

        testActor.Stop();
        testActor.Start();
        IMS_INFO(console, "Start/Stop test: %s\n", (testActor.Execute()) ? "PASSED" : "FAILED");
    }


    {
        IMSTest_Actor testActor(20);

        testActor.Stop();
        testActor.Stop();
        testActor.Stop();
        testActor.Start();
        testActor.Start();
        IMS_INFO(console, "Start/Stop odd test: %s\n", (testActor.Execute()) ? "PASSED" : "FAILED");
    }



    IIMSActor* dispatcher = objectPool.GetActor(ActorType_Dispatcher);
    console->SetVerbosityLevel(VerbosityLevel_Trace);

    IMSJob* job = objectPool.CreateJob();
    job->SetRequestType(RequestType_Render);
    job->SetProtocolType(ProtocolType_ZeroMQ);



    dispatcher->Submit(job);

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));


    objectPool.DeleteJob(job);



    //IMSTestActorWorkflow testActorWorkFlow(10);
    //
    //printf("Actor Flow Test (Regular): %s\n", testActorWorkFlow.TestRegularFlow() ? "SUCCESS" : "FAILURE");
    //printf("Actor Flow Test (Abort): %s\n", testActorWorkFlow.TestAbortFlow() ? "SUCCESS" : "FAILURE");


    //TestIMSPrint();
    //IMSTest_ActorFlow();


    // Possible Tests
    //  - IMSPrint
    //      - Test output of each MACRO (randomize thread time)
    //      - Validate verbosity level check
    //      
    //  - IMSActor
    //      - Test that number of send jobs accepted back (all job state updated)
    //      - Test Stop(). Suspend worker thread and Stop. Number of messages should be as number of accmplished jobs

    printf("\n");
    system("PAUSE");
}


void SetupDebugParameters()
{
    // ==================================================================================
    // Indicate to print out memory leak information on exit.
    // Instead of calling _CrtDumpMemoryLeaks at each exit point, we can set these flags.
    // only works in _DEBUG mode
    // https://msdn.microsoft.com/en-us/library/x98tx3cf(v=vs.120).aspx
#ifdef _WIN32
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
    // ==================================================================================
}

void PrintWelcomeMessage()
{
    IMSObjectPool& objectPool = IMSObjectPool::GetInstance();
    IMSPrint* console = objectPool.GetConsole();

    IMS_INFO(console, "\n");
    IMS_INFO(console, "==================================================\n");
    IMS_INFO(console, "  Idomoo Media Server ............... v%d.%02d.%02d.%d\n",
        IMS_VERSION_MAJOR, IMS_VERSION_MINOR,
        IMS_VERSION_PATCH, IMS_VERSION_BUILD);
    IMS_INFO(console, "  Using SpaceEngine subsystem ....... v%d.%02d.%02d.%d\n",
        ENGINE_VERSION_MAJOR, ENGINE_VERSION_MINOR,
        ENGINE_VERSION_PATCH, ENGINE_VERSION_BUILD);
    IMS_INFO(console, "==================================================\n");
    IMS_INFO(console, "\n\n");

}




int main(int argc, char **argv) 
{
    IMSObjectPool& objectPool = IMSObjectPool::GetInstance();
    IMSPrint* console = objectPool.GetConsole();
    IMSConfig config;
    bool stopExecution = false;
    

    SetupDebugParameters();
    PrintWelcomeMessage();

    try {
        if (!clreader::ParseCommandLine(argc, argv, config)) {
            return -1;  // Command line parsing failed
        }

        if (config.helpScreen) {
            return 0;   // User requested only the help message
        }

        objectPool.Init(&config);

        //RunTests();

        //while (1) {}


        // ==================================================================================
        // Explicitly dispose the object pool due to a bug in VS2012-VS2013
        // The "join" enters a lock state when called after main is exited
        // https://connect.microsoft.com/VisualStudio/feedback/details/747145
        // https://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc

        objectPool.Dispose();
        // ==================================================================================
    }
    catch (...) {
        std::cout << "ERROR:  IMS Execution Failed!" << std::endl;
        return -1;
    }

    return 0;
}