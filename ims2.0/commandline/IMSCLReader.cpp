#include "IMSCLReader.h"


#include <stdint.h>

#include "boost/program_options.hpp"
#include "boost/program_options/errors.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"

#include "objectpool/IMSObjectPool.h"
#include "print/IMSPrint.h"
#include "global/IMSException.h"

#include "rapidjson/document.h"
#include "utils/IMSJsonReader.h"


namespace po = boost::program_options;

#define CONFIG_FILE_SUPPORTED_VERSION "2.0"

#define CONFIG_FILE_VERSION	   	    "version"

#define GENERIC                     "generic"
#define GENERIC_AUTOS				"autos"
#define GENERIC_LOG_PATH			"log"
#define GENERIC_AUTO_SHUTDOWN		"auto-shutdown"
#define GENERIC_CONSOLE_VERBOSITY	"console-verbosity"
#define GENERIC_LOG_VERBOSITY		"log-verbosity"

#define COMMAND_LINE		        "command-line"
#define COMMAND_LINE_ENABLE		    "enabled"

#define ZEROMQ				        "zeromq"
#define ZEROMQ_ENABLE			    "enabled"
#define ZEROMQ_JOBS_ADDR		    "jobs-socket-addr"
#define ZEROMQ_JOBS_BINDCONN	    "jobs-socket-bind-connect"
#define ZEROMQ_JOBS_TYPE		    "jobs-socket-type"
#define ZEROMQ_RESULTS_ADDR		    "results-socket-addr"
#define ZEROMQ_RESULTS_BINDCONN	    "results-socket-bind-connect"
#define ZEROMQ_RESULTS_TYPE		    "results-socket-type"

#define RABBITMQ			        "rabbitmq"
#define RABBITMQ_ENABLE			    "enabled"
#define RABBITMQ_HOST			    "host"
#define RABBITMQ_PORT			    "port"
#define RABBITMQ_USER			    "username"
#define RABBITMQ_PWD			    "password"
#define RABBITMQ_RQ				    "render-queue"
#define RABBITMQ_DQ				    "done-queue"
#define RABBITMQ_PREFETCH		    "prefetch"

#define ENCODERS                    "encoders"
#define ENCODERS_NVENC              "encoder-nvenc"
#define ENCODERS_TGA                "encoder-tga"
#define ENCODERS_YUV                "encoder-yuv"
#define ENCODERS_NVENC_CONFIG_PATH	"nvenc-config-path"

#define SYSTEM                      "system"
#define SYSTEM_ASSET_MEMORY_LIMIT   "asset-memory-limit"
#define SYSTEM_ACTORS               "actors"       
#define ACTOR_PARSER                "parser"
#define ACTOR_TESTER                "tester"
#define ACTOR_NUM_THREADS           "num-worker-threads"


namespace idm {
    namespace ims {
        namespace clreader {


            bool ReadConfigFromFile(rapidjson::Document& root, IMSConfig& config)
            {
                // Read the version of the config file to choose the method of parsing.
                std::string version;
                if (json_reader::readStringProperty(root, CONFIG_FILE_VERSION, version)) {
                    if (version != CONFIG_FILE_SUPPORTED_VERSION) {
                        IMS_ERROR(IMSObjectPool::GetInstance().GetConsole(), "Unsupported configuration file (%s). Use (%s) version instead\n"
                            , version.c_str()
                            , CONFIG_FILE_SUPPORTED_VERSION);
                        return false;
                    }
                }

                bool status = true;

                // Read Generics part of the JSON
                if (root.HasMember(GENERIC))
                {
                    rapidjson::Value& generic = root[GENERIC];

                    json_reader::readStringProperty(generic, GENERIC_AUTOS,             config.Generic.AutosPath);
                    json_reader::readStringProperty(generic, GENERIC_LOG_PATH,          config.Generic.LogPath);
                    json_reader::readBoolProperty(generic,   GENERIC_AUTO_SHUTDOWN,     config.Generic.AutoShutdown);
                    json_reader::readIntProperty(generic,    GENERIC_CONSOLE_VERBOSITY, config.Generic.ConsoleVerbosity);
                    json_reader::readIntProperty(generic,    GENERIC_LOG_VERBOSITY,     config.Generic.LogVerbosity);
                }

                // Read CLI part of the JSOn
                if (root.HasMember(COMMAND_LINE))
                {
                    rapidjson::Value& cli = root[COMMAND_LINE];
                    json_reader::readBoolProperty(cli, COMMAND_LINE_ENABLE, config.CLI.Enable);
                }

                // Read the RabbitMQ part of the JSON
                if (root.HasMember(ZEROMQ)) 
                {
                    rapidjson::Value& ZeroMQ = root[ZEROMQ];
                    status = json_reader::readBoolProperty(ZeroMQ, ZEROMQ_ENABLE, config.ZMQ.Enable);

                    if (status && config.ZMQ.Enable)
                    {
                        json_reader::readStringProperty(ZeroMQ, ZEROMQ_JOBS_ADDR,         config.ZMQ.JobsSockAddr);
                        json_reader::readBoolProperty(ZeroMQ,   ZEROMQ_JOBS_BINDCONN,     config.ZMQ.JobsSockBindConnect);
                        json_reader::readIntProperty(ZeroMQ,    ZEROMQ_JOBS_TYPE,         config.ZMQ.JobsSockType);            
                        json_reader::readStringProperty(ZeroMQ, ZEROMQ_RESULTS_ADDR,      config.ZMQ.ResultsSockAddr);          
                        json_reader::readBoolProperty(ZeroMQ,   ZEROMQ_RESULTS_BINDCONN,  config.ZMQ.ResultsSockBindConnect);
                        json_reader::readIntProperty(ZeroMQ,    ZEROMQ_RESULTS_TYPE,      config.ZMQ.ResultsSockType); 
                    }
                }

                // Read the RabbitMQ part of the JSON
                if (root.HasMember(RABBITMQ)) 
                {
                    rapidjson::Value& Rabbit = root[RABBITMQ];
                    status = json_reader::readBoolProperty(Rabbit, RABBITMQ_ENABLE, config.RMQ.Enable);

                    if (status && config.RMQ.Enable)
                    {
                        json_reader::readStringProperty(Rabbit, RABBITMQ_HOST,     config.RMQ.HostName);
                        json_reader::readIntProperty(Rabbit,    RABBITMQ_PORT,     config.RMQ.HostPort);
                        json_reader::readStringProperty(Rabbit, RABBITMQ_USER,     config.RMQ.UserName);
                        json_reader::readStringProperty(Rabbit, RABBITMQ_PWD,      config.RMQ.Password);
                        json_reader::readStringProperty(Rabbit, RABBITMQ_RQ,       config.RMQ.RenderQueue);
                        json_reader::readStringProperty(Rabbit, RABBITMQ_DQ,       config.RMQ.DoneQueue);
                        json_reader::readIntProperty(Rabbit,    RABBITMQ_PREFETCH, config.RMQ.PrefetchCount); 
                    }

                }

                // Read the Encoders part of the JSON
                if (root.HasMember(ENCODERS))
                {
                    rapidjson::Value& encoders = root[ENCODERS];

                    json_reader::readBoolProperty(encoders, ENCODERS_NVENC, config.Encoders.EncoderNVENC);
                    json_reader::readBoolProperty(encoders, ENCODERS_TGA, config.Encoders.EncoderTGA);
                    json_reader::readBoolProperty(encoders, ENCODERS_YUV, config.Encoders.EncoderYUV);
                    json_reader::readStringProperty(encoders, ENCODERS_NVENC_CONFIG_PATH, config.Encoders.NVENCConfigPath);

                    if (!config.Encoders.EncoderNVENC && !config.Encoders.EncoderTGA && !config.Encoders.EncoderYUV)
                    {
                        config.Encoders.DummyEncoder = true;
                    }
                }


                // Read the Encoders part of the JSON
                if (root.HasMember(SYSTEM)) 
                {
                    rapidjson::Value& system = root[SYSTEM];
                    json_reader::readIntProperty(system, SYSTEM_ASSET_MEMORY_LIMIT, config.System.AssetMemoryLimit);

                    if (system.HasMember(SYSTEM_ACTORS)) {
                        rapidjson::Value& actors = system[SYSTEM_ACTORS];

                        json_reader::readIntProperty(actors[ACTOR_PARSER], ACTOR_NUM_THREADS, config.System.Actors.Parser.NumThreads);
                        json_reader::readIntProperty(actors[ACTOR_TESTER], ACTOR_NUM_THREADS, config.System.Actors.Tester.NumThreads);
                    }
                }
            
                return true;
            }


            bool ParseCommandLine(int argc, char ** argv, IMSConfig& config)
            {
                IMSObjectPool& objectPool = IMSObjectPool::GetInstance();
                IMSPrint* console = objectPool.GetConsole();

                po::options_description	generic("Generic Options");
                generic.add_options()
                    ("help,?", "Displays this help message.")
                    ("config", po::value<std::string>(&config.Generic.ConfigFilePath), "Path to optional JSON configuration file.")
                    ("log", po::value<std::string>(&config.Generic.LogPath)->required()->default_value(config.Generic.LogPath), "Path to the log file")
                    ("autos", po::value<std::string>(&config.Generic.AutosPath), "Path to a file containing a list of paths to be rendered automatically at the start of the program [optional]")
                    ("auto-shutdown", "Shuts down the ims server after rendering the files from the 'autos' path.");


                po::options_description	encoders("Encoders");
                encoders.add_options()
                    ("encoder-nvenc", po::value<bool>(&config.Encoders.EncoderNVENC)->required()->default_value(config.Encoders.EncoderNVENC), "NVENC Encoder enable/disable")
                    ("encoder-tga", po::value<bool>(&config.Encoders.EncoderTGA)->default_value(config.Encoders.EncoderTGA), "TGA Encoder enable/disable")
                    ("encoder-yuv", po::value<bool>(&config.Encoders.EncoderYUV)->default_value(config.Encoders.EncoderYUV), "YUV Encoder enable/disable")
                    ("nvenc", po::value<std::string>(&config.Encoders.NVENCConfigPath)->required()->default_value(config.Encoders.NVENCConfigPath), "Path to the NVENC configuration xml file");


                po::options_description	cli("Command Line Protocol");
                cli.add_options()
                    ("cmd-disable", "Turns off the command line listener");


                po::options_description	rmq("Rabbit MQ Protocol");
                rmq.add_options()
                    ("rmq-disable", "Turns off the RabbitMQ listener")
                    ("rmq-host", po::value<std::string>(&config.RMQ.HostName)->default_value(config.RMQ.HostName), "Host name of the RabbitMQ server")
                    ("rmq-port", po::value<uint32_t>(&config.RMQ.HostPort)->default_value(config.RMQ.HostPort), "Port on the RabbitMQ server")
                    ("rmq-user", po::value<std::string>(&config.RMQ.UserName)->required()->default_value(config.RMQ.UserName), "Username for RabbitMQ access")
                    ("rmq-pwd", po::value<std::string>(&config.RMQ.Password)->required()->default_value(config.RMQ.Password), "Password for RabbitMQ access")
                    ("rmq-render-queue", po::value<std::string>(&config.RMQ.RenderQueue)->default_value(config.RMQ.RenderQueue), "Name of the RabbitMQ render jobs queue")
                    ("rmq-done-queue", po::value<std::string>(&config.RMQ.DoneQueue)->default_value(config.RMQ.DoneQueue), "Name of the RabbitMQ finished jobs queue")
                    ("rmq-prefetch", po::value<uint32_t>(&config.RMQ.PrefetchCount)->required()->default_value(config.RMQ.PrefetchCount), "Prefetch count for RabbitMQ. Allows pulling this many items off of the queue simultaneously (i.e. before ack()ing previous jobs).");

                po::options_description	zmq("Zero MQ Protocol");
                zmq.add_options()
                    ("zmq-disable", "Turns off the ZeroMQ listener")
                    ("zmq-jobs-addr", po::value<std::string>(&config.ZMQ.JobsSockAddr)->default_value(config.ZMQ.JobsSockAddr), "Address of the jobs endpoint for ZeroMQ communication. Takes ZeroMQ form <protocol>://<ip>:<port>")
                    ("zmq-jobs-bind-connect", po::value<bool>(&config.ZMQ.JobsSockBindConnect)->default_value(config.ZMQ.JobsSockBindConnect), "Set to 1 if IMS's ZeroMQ jobs socket should bind to the jobs address, 0 if it should connect.")
                    ("zmq-jobs-type", po::value<uint32_t>(&config.ZMQ.JobsSockType)->default_value(config.ZMQ.JobsSockType), "Enumerated value of the ZeroMQ socket type for the jobs socket. Available types are:\n"
                    "PAIR:     0\nPUB:      1\nSUB:      2\nREQ:      3\nREP:      4\nDEALER:   5\nROUTER:   6\nPULL:     7\nPUSH:     8\nXPUB:     9\nXSUB:    10\nXSTREAM: 11")
                    ("zmq-results-addr", po::value<std::string>(&config.ZMQ.ResultsSockAddr)->default_value(config.ZMQ.ResultsSockAddr), "Address of the results endpoint for ZeroMQ communication. Takes ZeroMQ form <protocol>://<ip>:<port>")
                    ("zmq-results-bind-connect", po::value<bool>(&config.ZMQ.ResultsSockBindConnect)->default_value(config.ZMQ.ResultsSockBindConnect), "Set to 1 if IMS's ZeroMQ jobs socket should bind to the results address, 0 if it should connect.")
                    ("zmq-results-type", po::value<uint32_t>(&config.ZMQ.ResultsSockType)->default_value(config.ZMQ.ResultsSockType), "Enumerated value of the ZeroMQ socket type for the results socket. See entry for zmq-jobs-type for the list of available types.");

                po::options_description	system("system");
                system.add_options()
                    ("asset-memory-limit", po::value<uint32_t>(&config.System.AssetMemoryLimit)->required()->default_value(config.System.AssetMemoryLimit), "Number of entries in the asset cache for fonts.")
                    ("actor-parser-num-threads", po::value<uint32_t>(&config.System.Actors.Parser.NumThreads)->default_value(config.System.Actors.Parser.NumThreads), "Number of worker thread in the Parser actor")
                    ("actor-tester-num-threads", po::value<uint32_t>(&config.System.Actors.Tester.NumThreads)->default_value(config.System.Actors.Tester.NumThreads), "Number of worker thread in the Tester actor");


                // Use this one to read the options from the command line
                po::options_description commandline;
                commandline.add(generic).add(encoders).add(cli).add(rmq).add(zmq).add(system);

                // Use this one to display the help screen to the user
                po::options_description visible;
                visible.add(generic).add(encoders).add(cli).add(rmq).add(zmq);



                // parse command line options
                po::variables_map vm;
                try
                {
                    po::store(
                        po::command_line_parser(argc, argv)
                        .options(commandline)
                        .style(po::command_line_style::unix_style | po::command_line_style::allow_long_disguise)
                        .run(),
                        vm);

                    po::notify(vm);

                }
                catch (const po::required_option& e)
                {
                    IMS_ERROR(console, "Missing option: %s\n", e.get_option_name());
                    (*console) << visible << std::endl;

                    return false;
                }
                catch (const po::error& e)
                {
                    IMS_ERROR(console, "Missing option: %s\n", e.what());
                    (*console) << visible << std::endl;
                    return false;
                }


                // display help if they requested
                if (vm.count("help"))
                {
                    (*console) << visible << std::endl;
                    config.helpScreen = true;
                    return true;
                }


                config.RMQ.Enable = !(vm.count("no-rabbit")  > 0) && !(vm.count("rmq-disable") > 0);
                config.ZMQ.Enable = !(vm.count("no-zeromq")  > 0) && !(vm.count("zmq-disable") > 0);
                config.CLI.Enable = !(vm.count("no-cmdline") > 0) && !(vm.count("cmd-disable") > 0);
                config.Generic.AutoShutdown = (vm.count("auto-shutdown") > 0);

                // The configurations from the file will override the command line options
                if (vm.count("config") > 0)
                {
                    rapidjson::Document root;

                    if (json_reader::ReadJSONFile(config.Generic.ConfigFilePath.c_str(), root))
                    {
                        ReadConfigFromFile(root, config);
                    }
                }


                return true;

            }
        }
    }
}
