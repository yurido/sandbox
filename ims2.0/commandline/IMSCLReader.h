#ifndef IMS_CL_READER_H
#define IMS_CL_READER_H
#pragma  once

#include "global/IMSConfig.h"


namespace idm {
    namespace ims {
        namespace clreader {
            bool ParseCommandLine(int argc, char ** argv, IMSConfig& config);
        }
    }
}

#endif // IMS_CL_READER_H

