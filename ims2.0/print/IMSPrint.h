#ifndef IMS_PRINT_H
#define IMS_PRINT_H
#pragma once
#include <ostream>
#include <fstream>
#include <mutex>
#include <atomic>
#include <sstream>


#ifdef _WIN32
    #ifndef __func__
        #define __func__  __FUNCTION__
    #endif//__func__
#endif

namespace idm
{
    namespace ims
    {
        /*!
         * \brief Possible output destinations
         */
        typedef enum : uint8_t {
            OutDestination_Console,
            OutDestination_File

        } EOutDestination;


        /*!
        * \brief Verbosity level
        */
        typedef enum : uint8_t {
            VerbosityLevel_Silent  = 0,
            VerbosityLevel_Trace   = 1,
            VerbosityLevel_Info    = 2,
            VerbosityLevel_Warning = 3,
            VerbosityLevel_Profile = 4,
            VerbosityLevel_Error   = 5,
            VerbosityLevel_Fatal   = 6,
        } EVerbosityLevel;


        typedef enum : uint8_t {
            ConsoleColor_White,
            ConsoleColor_Red,
            ConsoleColor_Green,
            ConsoleColor_Blue
        } EConsoleColor;



        /*!
         * \brief printf style output macros
         */
        #define IMS_TRACE(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Trace,  __VA_ARGS__); \
        } while (0)

        #define IMS_INFO(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Info,  __VA_ARGS__); \
        } while (0)

        #define IMS_WARNING(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Warning,  __VA_ARGS__); \
        } while (0)

        #define IMS_PROFILE(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Profile,  __VA_ARGS__); \
        } while (0)

        #define IMS_ERROR(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Error,  __VA_ARGS__); \
        } while (0)

        #define IMS_FATAL(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Fatal,  __VA_ARGS__); \
        } while (0)


        /*!
        * \brief printf style output macros with extra debug parameters (file, func, line)
        */

        #define IMS_TRACE_FULL(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Trace, "TRACE| %s (%d):  %s() | ", __FILE__, __LINE__, __func__); \
            (dest)->Write(VerbosityLevel_Trace, __VA_ARGS__); \
        } while (0)

        #define IMS_INFO_FULL(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Info, "INFO| %s (%d):  %s() | ", __FILE__, __LINE__, __func__); \
            (dest)->Write(VerbosityLevel_Info, __VA_ARGS__); \
        } while (0)

        #define IMS_WARNING_FULL(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Warning, "WARNING| %s (%d):  %s() | ", __FILE__, __LINE__, __func__); \
            (dest)->Write(VerbosityLevel_Warning, __VA_ARGS__); \
        } while (0)

        #define IMS_PROFILE_FULL(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Profile, "PROFILE| %s (%d):  %s() | ", __FILE__, __LINE__, __func__); \
            (dest)->Write(VerbosityLevel_Profile, __VA_ARGS__); \
        } while (0)

        #define IMS_ERROR_FULL(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Error, "ERROR| %s (%d):  %s() | ", __FILE__, __LINE__, __func__); \
            (dest)->Write(VerbosityLevel_Error, __VA_ARGS__); \
        } while (0)

        #define IMS_FATAL_FULL(dest, ...) do { \
            (dest)->Write(VerbosityLevel_Fatal, "FATAL| %s (%d):  %s() | ", __FILE__, __LINE__, __func__); \
            (dest)->Write(VerbosityLevel_Fatal, __VA_ARGS__); \
        } while (0)



        /*!
         * \class IMSPrint
         *
         * \brief This class is a thread safe abstraction for console
         *        or file output. It can be used to synchronized 
         *        output to std::cout or any propriatery file. 
         *        If existing file provided - it will be overridden. 
         *
         * \author yurido
         * \date August 2017
         */
        class IMSPrint
        {
        private:
            using endl_type = std::ostream&(std::ostream&);

        public:
            /*!
             * \brief Default constructor
             *
             * \param[in] destination - output destination of the output. Can be console or file
             * \param[in] level - verbosity level. Only message above the verbosity level are printed
             * \param[in] filepath - if the destination is file, it's the path to the output file
             */
            IMSPrint(
                EOutDestination destination = OutDestination_Console, 
                EVerbosityLevel level = VerbosityLevel_Warning,
                const std::string& filepath = std::string());


            /*!
             * \brief default destructor
             */
            ~IMSPrint();


            /*!
             * \brief Set new output file stream
             *
             * \param[in] path to the file location
             * \note if file already exists, it will be overridden (throws exception on failure)
             */
            void SetOutputFileStream(const std::string& filepath);


            /*!
             * \brief Set new verbosity level
             */
            void SetVerbosityLevel(EVerbosityLevel level);


            /*!
             * \brief Returns verbosity level
             *
             * \return Verbosity level
             */
            EVerbosityLevel GetVerbosityLevel();


            /*!
             * \brief Prints current time in human readable format
             *
             * \param[in] additional message to add for the print out
             * \param[out]
             * \return
             */
            //void WriteTime(const char* fmt, ...);


            /*!
             * \brief printf style output function that accepts unlimited number of arguments
             *
             * \param[in] level - verbosity level
             * \param[in] fmt - output format
             * \param[in] ... - any additional arguments as expected by the format
             */
            void Write(EVerbosityLevel level, const char* fmt, ...);


            /*!
             * \brief Overload << operator to support C++ syntax
             *
             * \param[in]
             * \param[out]
             * \return
             */
            template<typename T>
            IMSPrint& operator<< (const T & data)
            {
                std::lock_guard<std::mutex> lk(m_outputMutex);

                m_outStream << data;
                return (*this);
            }

            IMSPrint& operator<<(endl_type endl)
            {
                std::lock_guard<std::mutex> lk(m_outputMutex);

                m_outStream << endl;
                return (*this);
            }
            
        private:
            
            std::atomic<EVerbosityLevel>   m_verbLevel;
            static std::mutex              m_outputMutex;
            std::ofstream                  m_outStream;
            std::ostringstream buffer_;

            std::ofstream                  m_fileStream; // Output file stream
        public:
            void SetColor(EConsoleColor color);
        };
    }
}


#endif // IMS_PRINT_H
