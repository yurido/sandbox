#include "IMSPrint.h"
#include <iostream>
#include "utils/IDMFileSysUtils.h"
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/operations.hpp"
#include <mutex>
#include <chrono>
#include <stdarg.h>

#ifdef _WIN32
#include <Windows.h>
#endif

namespace idm
{
    namespace ims
    {
        // Initialize static mutex variable
        std::mutex IMSPrint::m_outputMutex;

        void IMSPrint::SetColor(EConsoleColor color)
        {
#ifdef _WIN32
            int attributes = 0;
            if      (ConsoleColor_White == color) { attributes = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE; }
            else if (ConsoleColor_Red   == color) { attributes = FOREGROUND_RED;   }
            else if (ConsoleColor_Green == color) { attributes = FOREGROUND_GREEN; }
            else if (ConsoleColor_Blue  == color) { attributes = FOREGROUND_BLUE;  }
            else  { return; }

            HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
            SetConsoleTextAttribute(hStdOut, attributes) ? true : false;
#else
            // not yet implemented
            (void)color;
#endif
        }

        // Utility Macro: verbosity level check
        #define ASSERT_VERBOSITY_LEVEL(level)  if ((m_verbLevel == VerbosityLevel_Silent) || (level) < m_verbLevel) {return;}

        // Max output string length
        #define MAX_OUTPUT_STRING_LENGTH 4096

        IMSPrint::IMSPrint(
            EOutDestination destination /*= OutDestination_Console*/, 
            EVerbosityLevel level /*= VerbosityLevel_Warning*/, 
            const std::string& filepath /*= std::string()*/)
        {
            // Redirect output to the std::cout
            m_outStream.set_rdbuf(std::cout.rdbuf());
            m_verbLevel = level;

            if (destination == OutDestination_File) {
                SetOutputFileStream(filepath);
            }
        }

        IMSPrint::~IMSPrint()
        {
            if (m_fileStream.is_open()) {
                m_fileStream.close();
            }
        }

        void IMSPrint::SetOutputFileStream(const std::string& filepath)
        {
            // Close old stream
            if (m_fileStream.is_open()) {
                m_fileStream.close();
            }

            // Open new stream
            if (!idm::FileSysUtils::OpenOuputStream(filepath.c_str(), m_fileStream)) 
            {
                std::cout << "Failed to open file stream at location: " << filepath << std::endl;
                throw "File open failure";
            }

            // Redirect output to the file stream
            m_outStream.set_rdbuf(m_fileStream.rdbuf());
        }


        void IMSPrint::SetVerbosityLevel(EVerbosityLevel level)
        {
            m_verbLevel = level;
        }

        EVerbosityLevel IMSPrint::GetVerbosityLevel()
        {
            return m_verbLevel;
        }

        //void IMSPrint::WriteTime(const char* fmt, ...)
        //{
        //    ASSERT_VERBOSITY_LEVEL(VerbosityLevel_Profile);

        //    std::lock_guard<std::mutex> lk(m_outputMutex);

        //    //set time_point to current time
        //    auto time_point = std::chrono::system_clock::now();
        //    auto ttp = std::chrono::system_clock::to_time_t(time_point);
        //    std::string timeStr = std::string(std::ctime(&ttp));
        //    timeStr = timeStr.substr(0, timeStr.length() - 1);


        //    char buffer[MAX_OUTPUT_STRING_LENGTH];
        //    va_list arg;
        //    va_start(arg, fmt);
        //    std::vsnprintf(buffer, MAX_OUTPUT_STRING_LENGTH, fmt, arg);
        //    va_end(arg);

        //    m_outStream << timeStr << ": " << buffer;
        //}

        void IMSPrint::Write(EVerbosityLevel level, const char* fmt, ...)
        {
            // Don't print if verbosity level is to small
            ASSERT_VERBOSITY_LEVEL(level);

            std::lock_guard<std::mutex> lk(m_outputMutex);

            char buffer[MAX_OUTPUT_STRING_LENGTH];
            va_list arg;
            va_start(arg, fmt);
            std::vsnprintf(buffer, MAX_OUTPUT_STRING_LENGTH, fmt, arg);
            va_end(arg);

            m_outStream << buffer;
        }

    }
}